from flask import Flask
from flask.ext.admin.contrib import sqla
from flask_admin import Admin
from sqlalchemy.orm import sessionmaker
from datetime import datetime, timedelta
import os
import jwt
import json
import requests
from functools import wraps
from urlparse import parse_qs, parse_qsl, urlsplit
from urllib import urlencode
from flask import Flask, g, send_file, request, redirect, url_for, jsonify
#from flask.ext.sqlalchemy import SQLAlchemy
from tornado.web import HTTPError
from werkzeug.security import generate_password_hash, check_password_hash
from requests_oauthlib import OAuth1
from jwt import DecodeError, ExpiredSignature
from models import User, engine

current_path = os.path.dirname(__file__)
client_path = os.path.abspath(os.path.join(current_path, '..', '..', 'client'))

app = Flask(__name__, static_url_path='', static_folder=client_path)

#app.config.from_object('config')
app.config.from_pyfile('./config.py')
app.config['SECRET_KEY'] = '123456790'

Session = sessionmaker(bind=engine)
session = Session()

"""
#
# START ADMIN
#
"""
class UserAdmin(sqla.ModelView):
    column_display_pk = True
    form_columns = ['id', 'username', 'first_name', 'last_name', 'email', 'description']
    column_exclude_list = ['followers', 'following',]


admin = Admin(app, name='Contter.com', template_mode='bootstrap3')
admin.add_view(UserAdmin(User, session))
"""
#
# END ADMIN
#
"""

@app.route('/flask')
def hello_world():
  return 'This comes from Flask ^_^'

"""
#
# START AUTH
#
"""

def create_token(user):
    payload = {
        'sub': user.id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    token = jwt.encode(payload, app.config['TOKEN_SECRET'])
    return token.decode('unicode_escape')


def parse_token(req):
    token = req.headers.get('Authorization').split()[1]
    return jwt.decode(token, app.config['TOKEN_SECRET'])

def w_parse_token(req):
    token = req.cookies.get('tokenSession').value[9:]
    return jwt.decode(token, app.config['TOKEN_SECRET'])

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.headers.get('Authorization'):
            response = jsonify(message='Missing authorization header')
            response.status_code = 401
            return response

        try:
            print('req', request)
            payload = parse_token(request)
        except DecodeError:
            response = jsonify(message='Token is invalid')
            response.status_code = 401
            return response
        except ExpiredSignature:
            response = jsonify(message='Token has expired')
            response.status_code = 401
            return response

        g.user_id = payload['sub']

        return f(*args, **kwargs)

    return decorated_function


def authenticateded(method):
    """Decorate methods with this to require that the user be logged in.

    If the user is not logged in, they will be redirected to the configured
    `login url <RequestHandler.get_login_url>`.

    If you configure a login url with a query parameter, Tornado will
    assume you know what you're doing and use it as-is.  If not, it
    will add a `next` parameter so the login page knows where to send
    you once you're logged in.
    """
    wraps(method)
    def wrapper(self, *args, **kwargs):
        print('self.request.headers.get(Authorization)', self.request.headers.get('Authorization'))
        if not self.request.headers.get('Authorization'):
            """
            if self.request.method in ("GET", "HEAD"):
                url = self.get_login_url()
                if "?" not in url:
                    if urlsplit(url).scheme:
                        # if login url is absolute, make next absolute too
                        next_url = self.request.full_url()
                    else:
                        next_url = self.request.uri
                    url += "?" + urlencode(dict(next=next_url))
                self.redirect(url)
                return
            """
            raise HTTPError(403)
        try:
             payload = parse_token(self.request)
        except DecodeError:
            pass
            #response = jsonify(message='Token is invalid')
            #response.status_code = 401
            #return response
        except ExpiredSignature:
            pass
            #response = jsonify(message='Token has expired')
            #response.status_code = 401
            #return response
        self.current_user = payload['sub']
        return method(self, *args, **kwargs)
    return wrapper

def anonymous_authenticateded(method):
    """Decorate methods with this to require that the user be logged in.

    If the user is not logged in, they will be redirected to the configured
    `login url <RequestHandler.get_login_url>`.

    If you configure a login url with a query parameter, Tornado will
    assume you know what you're doing and use it as-is.  If not, it
    will add a `next` parameter so the login page knows where to send
    you once you're logged in.
    """
    wraps(method)
    def wrapper(self, *args, **kwargs):
        print('self.request.headers.get(Authorization)', self.request.headers.get('Authorization'))
        if not self.request.headers.get('Authorization'):
            """
            if self.request.method in ("GET", "HEAD"):
                url = self.get_login_url()
                if "?" not in url:
                    if urlsplit(url).scheme:
                        # if login url is absolute, make next absolute too
                        next_url = self.request.full_url()
                    else:
                        next_url = self.request.uri
                    url += "?" + urlencode(dict(next=next_url))
                self.redirect(url)
                return
            """
            self.current_user = None
            return method(self, *args, **kwargs)
        try:
             payload = parse_token(self.request)
        except DecodeError:
            pass
            #response = jsonify(message='Token is invalid')
            #response.status_code = 401
            #return response
        except ExpiredSignature:
            pass
            #response = jsonify(message='Token has expired')
            #response.status_code = 401
            #return response
        self.current_user = payload['sub']
        return method(self, *args, **kwargs)
    return wrapper

def w_authenticateded(method):
    wraps(method)
    def wrapper(self, *args, **kwargs):
        if not self.request.cookies.get('tokenSession'):
            raise HTTPError(403)
        try:
             payload = w_parse_token(self.request)
        except DecodeError:
            pass
            #response = jsonify(message='Token is invalid')
            #response.status_code = 401
            #return response
        except ExpiredSignature:
            pass
            #response = jsonify(message='Token has expired')
            #response.status_code = 401
            #return response
        self.current_user = payload['sub']
        return method(self, *args, **kwargs)
    return wrapper

# Routes

#@app.route('/')
#def index():
#    return send_file(os.path.join(client_path, 'index.html'))


@app.route('/api/me')
@login_required
def me():
    user = session.query(User).filter(User.id == g.user_id).first()
    print('user', user.to_json())
    return jsonify(user.to_json())


@app.route('/api/auth/login', methods=['POST'])
def login():
    user = User.query.filter_by(email=request.json['email']).first()
    if not user or not user.check_password(request.json['password']):
        response = jsonify(message='Wrong Email or Password')
        response.status_code = 401
        return response
    token = create_token(user)
    return jsonify(token=token)


@app.route('/api/auth/signup', methods=['POST'])
def signup():
    user = User(email=request.json['email'], password=request.json['password'])
    session.add(user)
    session.commit()
    token = create_token(user)
    return jsonify(token=token)


@app.route('/api/auth/facebook', methods=['POST'])
def facebook():
    access_token_url = 'https://graph.facebook.com/v2.3/oauth/access_token'
    graph_api_url = 'https://graph.facebook.com/v2.3/me'

    params = {
        'client_id': request.json['clientId'],
        'redirect_uri': request.json['redirectUri'],
        'client_secret': app.config['FACEBOOK_SECRET'],
        'code': request.json['code']
    }

    # Step 1. Exchange authorization code for access token.
    r = requests.get(access_token_url, params=params)
    access_token = dict(parse_qsl(r.text))

    # Step 2. Retrieve information about the current user.
    r = requests.get(graph_api_url, params=access_token)
    profile = json.loads(r.text)

    # Step 3. (optional) Link accounts.
    if request.headers.get('Authorization'):
        user = User.query.filter_by(facebook=profile['id']).first()
        if user:
            response = jsonify(message='There is already a Facebook account that belongs to you')
            response.status_code = 409
            return response

        payload = parse_token(request)

        user = User.query.filter_by(id=payload['sub']).first()
        if not user:
            response = jsonify(message='User not found')
            response.status_code = 400
            return response

        u = User(facebook=profile['id'], display_name=profile['name'])
        session.add(u)
        session.commit()
        token = create_token(u)
        return jsonify(token=token)

    # Step 4. Create a new account or return an existing one.
    user = User.query.filter_by(facebook=profile['id']).first()
    if user:
        token = create_token(user)
        return jsonify(token=token)

    u = User(facebook=profile['id'], display_name=profile['name'])
    session.add(u)
    session.commit()
    token = create_token(u)
    return jsonify(token=token)

@app.route('/api/auth/github', methods=['POST'])
def github():
    access_token_url = 'https://github.com/login/oauth/access_token'
    users_api_url = 'https://api.github.com/user'

    params = {
        'client_id': request.json['clientId'],
        'redirect_uri': request.json['redirectUri'],
        'client_secret': app.config['GITHUB_SECRET'],
        'code': request.json['code']
    }

    # Step 1. Exchange authorization code for access token.
    r = requests.get(access_token_url, params=params)
    access_token = dict(parse_qsl(r.text))
    headers = {'User-Agent': 'Satellizer'}

    # Step 2. Retrieve information about the current user.
    r = requests.get(users_api_url, params=access_token, headers=headers)
    profile = json.loads(r.text)

    # Step 3. (optional) Link accounts.
    if request.headers.get('Authorization'):
        user = User.query.filter_by(github=profile['id']).first()
        if user:
            response = jsonify(message='There is already a GitHub account that belongs to you')
            response.status_code = 409
            return response

        payload = parse_token(request)

        user = User.query.filter_by(id=payload['sub']).first()
        if not user:
            response = jsonify(message='User not found')
            response.status_code = 400
            return response

        u = User(github=profile['id'], display_name=profile['name'])
        session.add(u)
        session.commit()
        token = create_token(u)
        return jsonify(token=token)

    # Step 4. Create a new account or return an existing one.
    user = User.query.filter_by(github=profile['id']).first()
    if user:
        token = create_token(user)
        return jsonify(token=token)

    u = User(github=profile['id'], display_name=profile['name'])
    session.add(u)
    session.commit()
    token = create_token(u)
    return jsonify(token=token)



@app.route('/api/auth/google', methods=['POST'])
def google():
    access_token_url = 'https://accounts.google.com/o/oauth2/token'
    people_api_url = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect'

    payload = dict(client_id=request.json['clientId'],
                   redirect_uri=request.json['redirectUri'],
                   client_secret=app.config['GOOGLE_SECRET'],
                   code=request.json['code'],
                   grant_type='authorization_code')

    # Step 1. Exchange authorization code for access token.
    r = requests.post(access_token_url, data=payload)
    token = json.loads(r.text)
    headers = {'Authorization': 'Bearer {0}'.format(token['access_token'])}

    # Step 2. Retrieve information about the current user.
    r = requests.get(people_api_url, headers=headers)
    profile = json.loads(r.text)

    user = User.query.filter_by(google=profile['sub']).first()
    if user:
        token = create_token(user)
        return jsonify(token=token)
    u = User(google=profile['sub'],
             display_name=profile['name'])
    session.add(u)
    session.commit()
    token = create_token(u)
    return jsonify(token=token)


@app.route('/api/auth/linkedin', methods=['POST'])
def linkedin():
    access_token_url = 'https://www.linkedin.com/uas/oauth2/accessToken'
    people_api_url = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)'

    payload = dict(client_id=request.json['clientId'],
                   redirect_uri=request.json['redirectUri'],
                   client_secret=app.config['LINKEDIN_SECRET'],
                   code=request.json['code'],
                   grant_type='authorization_code')

    # Step 1. Exchange authorization code for access token.
    r = requests.post(access_token_url, data=payload)
    access_token = json.loads(r.text)
    params = dict(oauth2_access_token=access_token['access_token'],
                  format='json')

    # Step 2. Retrieve information about the current user.
    r = requests.get(people_api_url, params=params)
    profile = json.loads(r.text)

    user = User.query.filter_by(linkedin=profile['id']).first()
    if user:
        token = create_token(user)
        return jsonify(token=token)
    u = User(linkedin=profile['id'],
             display_name=profile['firstName'] + ' ' + profile['lastName'])
    session.add(u)
    session.commit()
    token = create_token(u)
    return jsonify(token=token)


@app.route('/api/auth/twitter', methods=['POST'])
def twitter():
    request_token_url = 'https://api.twitter.com/oauth/request_token'
    access_token_url = 'https://api.twitter.com/oauth/access_token'

    if request.json.get('oauth_token') and request.json.get('oauth_verifier'):
        auth = OAuth1(app.config['TWITTER_CONSUMER_KEY'],
                      client_secret=app.config['TWITTER_CONSUMER_SECRET'],
                      resource_owner_key=request.json.get('oauth_token'),
                      verifier=request.json.get('oauth_verifier'))
        r = requests.post(access_token_url, auth=auth)
        profile = dict(parse_qsl(r.text))

        user = session.query(User).filter(User.twitter == profile['user_id']).first()
        if user:
            if user.new:
                user.new = False
                session.commit()
            token = create_token(user)
            return jsonify(token=token)
        u = User(twitter=profile['user_id'],
                 display_name=profile['screen_name'])
        session.add(u)
        session.commit()
        token = create_token(u)
        return jsonify(token=token)
    else:
        oauth = OAuth1(app.config['TWITTER_CONSUMER_KEY'],
                       client_secret=app.config['TWITTER_CONSUMER_SECRET'],
                       callback_uri=app.config['TWITTER_CALLBACK_URL'])
        r = requests.post(request_token_url, auth=oauth)
        oauth_token = dict(parse_qsl(r.text))
        return jsonify(oauth_token)


"""
#
# END AUTH
#
"""