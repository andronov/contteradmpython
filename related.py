import string
from pprint import pprint

import nltk
import time
from difflib import SequenceMatcher
import operator
from nltk import TabTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sqlalchemy import desc, asc
from sqlalchemy.orm import sessionmaker
from textblob.en.taggers import NLTKTagger
from textblob.wordnet import Synset
from models import engine, User, BaseSite, UserWall, UserColumn, BaseLink, UserColumnLink, UserColumnSettings
from textblob import TextBlob

Session = sessionmaker(bind=engine)

session = Session()


stemmer = nltk.stem.porter.PorterStemmer()
remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)

def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]

'''remove punctuation, lowercase, stem'''
def normalize(text):
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))

vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')

def cosine_sim(text1, text2):
    tfidf = vectorizer.fit_transform([text1, text2])
    return ((tfidf * tfidf.T).A)[0,1]

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def related_to(item, items):
    start = time.time()
    title = item.title
    description = item.description
    txt = title + ' ' + description

    result = []
    results = []

    for it in items:
        #try:
            txt2 = it.title + ' ' + item.description
            #print txt
            #print txt2
            #z = cosine_sim(txt, txt2)
            z = similar(txt, txt2)
            results.append({'id': it.id, 'sim': z})
            #if z > 0.24:
                #result.append(it.id)
                #print txt2
        #except:
            #pass

    #session.commit()
    #session.close()
    print('results')
    print(results)
    sortlist = sorted(results, key=operator.itemgetter('sim'), reverse=True)
    print(sortlist)
    print('time', time.time() - start)

    return sortlist[:20]


def related_to_id(id):
    item = session.query(BaseLink).filter(BaseLink.id == id).first()
    title = item.title
    description = item.description
    txt = title + ' ' + description

    result = []

    items = session.query(BaseLink).filter(BaseLink.is_active == True, BaseLink.id != id).all().order_by(asc(UserColumnLink.created))
    i = 1
    for it in items:
        try:
            txt2 = it.title + ' ' + item.description
            #print txt
            #print txt2
            z = cosine_sim(txt, txt2)
            if z > 0.10:
                result.append(it.id)
                print txt2
            i += 1
            if i >= 5:
                break
        except:
            pass

    session.commit()
    session.close()

    return result

def related():
    item = session.query(BaseLink).filter(BaseLink.id == 328).first()
    title = item.title
    description = item.description
    txt = title + ' ' + description

    items = session.query(BaseLink).filter(BaseLink.is_active == True, BaseLink.id != 328).all().order_by(asc(UserColumnLink.created))
    for it in items:
        try:
            txt2 = it.title + ' ' + item.description
            #print txt
            #print txt2
            z = cosine_sim(txt, txt2)
            if z > 0.64:
                print txt2
        except:
            pass

    #print('txt', txt)
    #txt2 = 'Mike Piazza weighs in on waiting for Hall call, cap on plaque, back acne Former New York Mets catcher Mike Piazza shares his thoughts on waiting four years to get to the Hall of Fame, and PED questions.'
    #print cosine_sim(txt, txt2)


    """
    text = '''
    The titular threat of The Blob has always struck me as the ultimate movie
    monster: an insatiably hungry, amoeba-like mass able to penetrate
    virtually any safeguard, capable of--as a doomed doctor chillingly
    describes it--"assimilating flesh on contact.
    Snide comparisons to gelatin be damned, it's a concept with the most
    devastating of potential consequences, not unlike the grey goo scenario
    proposed by technological theorists fearful of
    artificial intelligence run rampant.
    '''

    text = txt

    nltk_tagger = NLTKTagger()
    tokenizer = TabTokenizer()
    blob = TextBlob(text, pos_tagger=nltk_tagger, tokenizer=tokenizer)


    print('words', blob.words)

    print('tags', blob.tags)


    print('noun_phrases', blob.noun_phrases)

    for sentence in blob.sentences:
        print(sentence.sentiment.polarity)
    """

    session.commit()
    session.close()
    pass

if __name__ == '__main__':
    related()
    pass
