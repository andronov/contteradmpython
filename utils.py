from urlparse import urlparse


def validate_url(link):
    check, o = False, None
    o = urlparse(link)
    print(o)
    if o.scheme and o.netloc:
        if o.scheme in ['http', 'https'] and len(o.netloc) > 0:
            check = True
            return check, o
    return check, o

def validate_wall_name(text):
    check = False
    if len(text) >= 1 and len(text) <= 11:
        check = True
    return check

def validate_word(text):
    return True