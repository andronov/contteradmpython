
collect = [
		{
			"name": "SPORTS",
			"id": 1,
			"sort": 1,
            "active": True,
			"walls": [
				{
					"id": 1,
					"name": "ESPN",
					"description": "Visit ESPN to get up-to-the-minute sports news coverage, scores, highlights and commentary for NFL, MLB, NBA, College Football, NCAA Basketball and more.",
					"image": "wl-wall-espn1.jpg",
					"thumbnail": "wl-wall-thumb-espn1.jpg",
					"columns": [
						{
							"id": 1,
							"name": "ESPN NEWS",
							"favicon": "http://a.espncdn.com/favicon.ico"
						},
						{
							"id": 2,
							"name": "ESPN NFL",
							"favicon": "http://a.espncdn.com/favicon.ico"
						},
						{
							"id": 3,
							"name": "ESPN NBA",
							"favicon": "http://a.espncdn.com/favicon.ico"
						},
						{
							"id": 4,
							"name": "ESPN MLB",
							"favicon": "http://a.espncdn.com/favicon.ico"
						},
						{
							"id": 5,
							"name": "ESPN NHL",
							"favicon": "http://a.espncdn.com/favicon.ico"
						}
					]
				},
				{
					"id": 2,
					"name": "HOCKEY",
					"description": "Column hockey.",
					"image": "wl-wall-hockey.jpg",
					"thumbnail": "wl-wall-thumb-hockey.jpg",
					"columns": [
						{
							"id": 6,
							"name": "NHL.COM",
							"favicon": "http://cdn.nhle.com/nhl/images/favicon.ico?v=9.11"
						}
					]
				}
			]
		},
		{
			"name": "STARTUPS",
			"id": 2,
			"sort": 2,
            "active": False,
			"walls": [
				{
					"id": 3,
					"name": "TOPTRENDS",
					"description": "Column news and headlines about startups from across the web.",
					"image": "wl-wall-startup.jpg",
					"thumbnail": "wl-wall-thumb-startup.jpg",
					"columns": [
						{
							"id": 9,
							"name": "VC.RU",
							"favicon": "https://vc.ru/static/main/img/favicon.ico?a96655d"
						},
						{
							"id": 10,
							"name": "BUSINESSINSIDER.COM",
							"favicon": "http://static3.businessinsider.com/assets/images/us/favicons/favicon.ico?v=zXXjpe0lwg"
						}
					]
				}
			]
		}
	]