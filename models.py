import enum
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
from sqlalchemy.orm import relationship, backref
from sqlalchemy_utils import ChoiceType, JSONType
from werkzeug.security import generate_password_hash, check_password_hash

engine = sa.create_engine('postgresql+psycopg2://postgres:rjierb18@localhost:5432/contest5', echo=False)

Base = declarative_base()

"""
#
# Abstract Base model
#
"""


class CommonRoutines(Base):
    __abstract__ = True

    is_active = sa.Column(sa.Boolean, unique=False, default=False)
    updated = sa.Column(sa.DateTime)
    created = sa.Column(sa.DateTime, default=sa.func.now())


"""
#
# User model
#
"""
user_followers = sa.Table('user_followers', Base.metadata,
                          sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                          sa.Column('user_base_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True, index=True),
                          sa.Column('user_to_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True))

user_following = sa.Table('user_following', Base.metadata,
                          sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                          sa.Column('user_base_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True, index=True),
                          sa.Column('user_to_id', sa.Integer, sa.ForeignKey('users.id'), nullable=True))


class User(CommonRoutines):
    TYPE_TYPES = [
        (1, u'public'),
        (2, u'private')
    ]
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.String)
    first_name = sa.Column(sa.String, nullable=True)
    last_name = sa.Column(sa.String, nullable=True)
    password = sa.Column(sa.String)
    email = sa.Column(sa.String, nullable=True)

    type = sa.Column(sa.Integer, nullable=True, default=1)

    display_name = sa.Column(sa.String(120), nullable=True)
    facebook = sa.Column(sa.String(120), nullable=True)
    github = sa.Column(sa.String(120), nullable=True)
    google = sa.Column(sa.String(120), nullable=True)
    linkedin = sa.Column(sa.String(120), nullable=True)
    twitter = sa.Column(sa.String(120), nullable=True)
    twitter_img = sa.Column(sa.String, nullable=True)
    pinterest = sa.Column(sa.String(120), nullable=True)
    instagram = sa.Column(sa.String(120), nullable=True)

    color = sa.Column(sa.String, nullable=True)

    description = sa.Column(sa.String, nullable=True)
    avatar = sa.Column(sa.String, nullable=True)

    extra = sa.Column(JSONType)

    new = sa.Column(sa.Boolean, unique=False, default=True)

    last_visit = sa.Column(sa.DateTime)
    city = sa.Column(sa.String(), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    official = sa.Column(sa.Boolean, unique=False, default=False)
    official_info = sa.Column(sa.String, nullable=True)

    # followers = relationship('UserWall', secondary=usercolumn_walls,
    #                     backref=backref('user_column', lazy='dynamic'))
    followers = relationship("User",
                             secondary=user_followers,
                             primaryjoin=id == user_followers.c.user_base_id,
                             secondaryjoin=id == user_followers.c.user_to_id,
                             backref=backref('user1', lazy='dynamic')
                             )

    following = relationship("User",
                             secondary=user_following,
                             primaryjoin=id == user_following.c.user_base_id,
                             secondaryjoin=id == user_following.c.user_to_id,
                             backref=backref('user2', lazy='dynamic')
                             )

    def __init__(self, email=None, password=None, display_name=None,
                 facebook=None, github=None, google=None, linkedin=None,
                 twitter=None):
        if email:
            self.email = email.lower()
        if password:
            self.set_password(password)
        if display_name:
            self.display_name = display_name
        if facebook:
            self.facebook = facebook
        if google:
            self.google = google
        if linkedin:
            self.linkedin = linkedin
        if twitter:
            self.twitter = twitter

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def name(self):
        try:
            name = self.first_name + ' ' + self.last_name
        except:
            name = self.first_name
        return name

    def to_json(self):
        return dict(id=self.id, email=self.email, displayName=self.display_name,
                    facebook=self.facebook, google=self.google,
                    linkedin=self.linkedin, twitter=self.twitter, new=self.new,
                    description=self.description, first_name=self.first_name, last_name=self.last_name,
                    name=self.name(), avatar=self.avatar)

    def __repr__(self):
        return '<User: {}>'.format(self.id)


"""
#
# UserNotification model
#
"""


class UserNotification(CommonRoutines):
    __tablename__ = 'user_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True)


"""
#
# BaseSite model
#
"""


class BaseSite(CommonRoutines):
    __tablename__ = 'base_site'

    id = sa.Column(sa.Integer, primary_key=True)

    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    url = sa.Column(sa.String, nullable=True)
    short_url = sa.Column(sa.String, nullable=True)
    rss = sa.Column(sa.String, nullable=True)  # url rss
    image = sa.Column(sa.String, nullable=True)
    favicon = sa.Column(sa.String, nullable=True)
    color = sa.Column(sa.String, nullable=True)

    parent = sa.Column(sa.Boolean, unique=False, default=True)
    parentid = sa.Column(sa.Integer, nullable=True)

    extra = sa.Column(JSONType)  # extra information site

    custom = sa.Column(sa.Boolean, default=True)


"""
#
# BaseLink model
#
"""


class BaseLink(CommonRoutines):
    __tablename__ = 'base_link'

    id = sa.Column(sa.Integer, primary_key=True)

    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=False)

    title = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    keywords = sa.Column(sa.String, nullable=True)

    url = sa.Column(sa.String, nullable=True)
    full_url = sa.Column(sa.String, nullable=True)  # url rss

    thumbnail = sa.Column(sa.String, nullable=True)
    extra_img = sa.Column(sa.String, nullable=True)
    small_img = sa.Column(sa.String, nullable=True)
    medium_img = sa.Column(sa.String, nullable=True)
    large_img = sa.Column(sa.String, nullable=True)
    site_img = sa.Column(sa.String, nullable=True)
    site_img_url = sa.Column(sa.String, nullable=True)
    json_img = sa.Column(JSONB, nullable=True)

    html = sa.Column(sa.String, nullable=True)

    counter = sa.Column(sa.Integer, default=0, nullable=True)

    tags = sa.Column(sa.String, nullable=True)

    show = sa.Column(sa.Boolean, unique=False, default=True)

    extra = sa.Column(JSONType)  # extra information link


"""
#
# UserWall model
#
"""


class UserWall(CommonRoutines):
    __tablename__ = 'user_wall'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    color = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)

    follow_user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    follow = sa.Column(sa.Boolean, unique=False, default=False)

    icon = sa.Column(sa.String, nullable=True)

    sort = sa.Column(sa.Integer, nullable=True)
    category_id = sa.Column(sa.Integer, nullable=True)

    is_top = sa.Column(sa.Boolean, unique=False, default=False)
    is_trends = sa.Column(sa.Boolean, unique=False, default=False)
    is_collections = sa.Column(sa.Boolean, unique=False, default=False)
    is_for_you = sa.Column(sa.Boolean, unique=False, default=False)

    thumbnail = sa.Column(sa.String, nullable=True)
    medium_img = sa.Column(sa.String, nullable=True)

    home = sa.Column(sa.Boolean, unique=False, default=False)


"""
#
# UserColumnLink model
#
"""


class UserColumnLink(CommonRoutines):
    __tablename__ = 'user_column_link'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    link_id = sa.Column(sa.Integer, sa.ForeignKey('base_link.id'), nullable=True)
    column_id = sa.Column(sa.Integer, nullable=True)

    recontent = sa.Column(sa.Boolean, unique=False, default=False)
    related = sa.Column(sa.Boolean, unique=False, default=False)
    word = sa.Column(sa.Boolean, unique=False, default=False)
    site = sa.Column(sa.Boolean, unique=False, default=True)
    show = sa.Column(sa.Boolean, unique=False, default=True)

    if_recontent = sa.Column(sa.Boolean, unique=False, default=False)
    if_favourite = sa.Column(sa.Boolean, unique=False, default=False)
    if_recontent_id = sa.Column(sa.Integer, nullable=True)
    if_favourite_id = sa.Column(sa.Integer, nullable=True)

    counter = sa.Column(sa.Integer, nullable=True)  # count perehodov

    favourite = sa.Column(sa.Boolean, unique=False, default=False)
    fav_add = sa.Column(sa.DateTime, nullable=True)

    related_children = sa.Column(sa.Integer, sa.ForeignKey('user_column_link.id'), nullable=True)


"""
#
# CategoryModel model
#
"""


class CategoryModel(CommonRoutines):
    TYPE_TYPES = [
        (1, u'base'),
        (2, u'for user'),
    ]
    __tablename__ = 'category_model'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    short_name = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.Integer, nullable=True, default=1)

    word_filter = sa.Column(sa.String, nullable=True)
    word_exclude = sa.Column(sa.String, nullable=True)
    tags = sa.Column(sa.String, nullable=True)

    extra = sa.Column(sa.String, nullable=True)  # to json

    color = sa.Column(sa.String, nullable=True)
    icon = sa.Column(sa.String, nullable=True)

    is_admin = sa.Column(sa.Boolean, unique=False, default=False)
    is_show = sa.Column(sa.Boolean, unique=False, default=False)

    is_top = sa.Column(sa.Boolean, unique=False, default=False)
    is_trends = sa.Column(sa.Boolean, unique=False, default=False)
    is_collections = sa.Column(sa.Boolean, unique=False, default=False)
    is_for_you = sa.Column(sa.Boolean, unique=False, default=False)

    sort = sa.Column(sa.Integer, nullable=True)


"""
#
# UserColumn model
#
"""
"""
usercolumn_walls = sa.Table('usercolumns_walls', Base.metadata,
                            sa.Column('user_wall_id', sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True),
                            sa.Column('usercolumn_wall_id', sa.Integer, sa.ForeignKey('user_column.id'),
                                      primary_key=True, nullable=True)
                            )
"""

usercolumn_walls = sa.Table('usercolumn_walls', Base.metadata,
                            sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                            sa.Column('user_wall_id', sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True,
                                      index=True),
                            sa.Column('usercolumn_wall_id', sa.Integer, sa.ForeignKey('user_column.id'), nullable=True))

userwall_sites = sa.Table('userwall_sites', Base.metadata,
                            sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                            sa.Column('userwall_wall_id', sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True,
                                      index=True),
                            sa.Column('userwall_site_id', sa.Integer, sa.ForeignKey('base_site.id'), nullable=True))

column_to_column = sa.Table('column_to_column', Base.metadata,
                            sa.Column('id', sa.Integer, primary_key=True, nullable=True),
                            sa.Column('column_base_id', sa.Integer, sa.ForeignKey('user_column.id'), nullable=True,
                                      index=True),
                            sa.Column('column_to_id', sa.Integer, sa.ForeignKey('user_column.id'), nullable=True))


class UserColumn(CommonRoutines):
    TYPE_TYPES = [
        (1, u'Site'),
        (2, u'Word'),
        (3, u'recontent'),
        (4, u'related'),
        (5, u'top'),
        (6, u'trends'),
        (7, u'recommend for you from top'),
        (8, u'collections'),
        (106, u'no show trends'),
    ]
    __tablename__ = 'user_column'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)

    column_to_rel_column = sa.Column(sa.Integer, nullable=True, default=None)

    related_link_id = sa.Column(sa.Integer, sa.ForeignKey('user_column_link.id'), nullable=True)

    walls = relationship('UserWall', secondary=usercolumn_walls,
                         backref=backref('user_column', lazy='dynamic'))

    columns = relationship("UserColumn",
                           secondary=column_to_column,
                           primaryjoin=id == column_to_column.c.column_base_id,
                           secondaryjoin=id == column_to_column.c.column_to_id,
                           backref=backref('columns1', lazy='dynamic')
                           )

    slug = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.Integer, nullable=True, default=1)

    follow_user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    follow = sa.Column(sa.Boolean, unique=False, default=False)

    color = sa.Column(sa.String, nullable=True)
    icon = sa.Column(sa.String, nullable=True)


"""
#
# Collections model
#
"""


class CollectionsModel(CommonRoutines):
    TYPE_TYPES = [
        (1, u'base'),
        (2, u'for user'),
    ]
    __tablename__ = 'collections_model'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    slug = sa.Column(sa.String, nullable=True)
    short_name = sa.Column(sa.String, nullable=True)
    name = sa.Column(sa.String, nullable=True)
    description = sa.Column(sa.String, nullable=True)
    type = sa.Column(sa.Integer, nullable=True, default=1)

    extra = sa.Column(sa.String, nullable=True)  # to json

    color = sa.Column(sa.String, nullable=True)
    icon = sa.Column(sa.String, nullable=True)

"""
#
# CategoryToModel model
#
"""


class CategoryToModel(CommonRoutines):
    __tablename__ = 'category_to_model'

    id = sa.Column(sa.Integer, primary_key=True)

    category_model_id = sa.Column(sa.Integer, sa.ForeignKey('category_model.id'), nullable=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    site_id = sa.Column(sa.Integer, sa.ForeignKey('base_site.id'), nullable=True)
    collection_id = sa.Column(sa.Integer, sa.ForeignKey('collections_model.id'), nullable=True)

    is_admin = sa.Column(sa.Boolean, unique=False, default=False)
    is_show = sa.Column(sa.Boolean, unique=False, default=False)

    is_top = sa.Column(sa.Boolean, unique=False, default=False)
    is_trends = sa.Column(sa.Boolean, unique=False, default=False)
    is_collections = sa.Column(sa.Boolean, unique=False, default=False)
    is_for_you = sa.Column(sa.Boolean, unique=False, default=False)

    extra = sa.Column(sa.String, nullable=True)  # to json

"""
#
# UserColumnSettings model
#
"""


class UserColumnSettings(CommonRoutines):
    SIZE_TYPES = [
        (200, u'size200'),
        (300, u'size300'),
        (400, u'size400'),
        (500, u'size500'),
        (600, u'size600'),
        (1000, u'size_other'),
    ]
    TYPE_TYPES = [
        (1, u'public'),
        (2, u'private'),
    ]
    __tablename__ = 'user_column_settings'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    word_filter = sa.Column(sa.String, nullable=True)
    word_exclude = sa.Column(sa.String, nullable=True)

    sort = sa.Column(sa.Integer, nullable=True)
    realtime = sa.Column(sa.Boolean, unique=False, default=False)

    size = sa.Column(ChoiceType(SIZE_TYPES, impl=sa.Integer()), nullable=True, default=300)
    type = sa.Column(ChoiceType(TYPE_TYPES, impl=sa.Integer()), nullable=True, default=1)

    def as_dict(self):
        return {'word_filter': self.get_word_filter(), 'word_exclude': self.get_word_exclude(), 'sort': self.sort,
                'size': self.size.code, 'type': self.type.code, 'realtime': self.realtime}

    def get_word_exclude(self):
        try:
            return self.word_exclude.split(', ')
        except:
            return []

    def get_word_filter(self):
        try:
            return self.word_filter.split(', ')
        except:
            return []


"""
#
# UserColumnNotification model
#
"""


class UserColumnNotification(CommonRoutines):
    __tablename__ = 'user_column_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True)  # day, week, month


"""
#
# UserWallNotification model
#
"""


class UserWallNotification(CommonRoutines):
    __tablename__ = 'user_wall_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)

    email = sa.Column(sa.String, nullable=True)
    noti_email = sa.Column(sa.Boolean, unique=False, default=False)
    noti_site = sa.Column(sa.Boolean, unique=False, default=True)
    noti_sound = sa.Column(sa.Boolean, unique=False, default=False)
    noti_digest = sa.Column(sa.Boolean, unique=False, default=False)

    interval_digest = sa.Column(sa.String, nullable=True)  # day, week, month


"""
class UserColumnWallSort(BaseModel):
    user = models.ForeignKey(User, null=True, blank=True, related_name='user_column_wall_sort_user_id', name='user')
    wall = models.ForeignKey(UserWall, blank=True, null=True, related_name='user_column_wall_sort_wall_id', name='wall')
    column = models.ForeignKey(UserColumn, blank=True, null=True, related_name='user_column_wall_sort_column_id',
                               name='column')

    sort = models.IntegerField(blank=True, null=True)


    class Meta:
        db_table = 'user_column_wall_sort'
"""


class UserColumnWallSort(CommonRoutines):
    __tablename__ = 'user_column_wall_sort'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    wall_id = sa.Column(sa.Integer, sa.ForeignKey('user_wall.id'), nullable=True)
    column_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    sort = sa.Column(sa.Integer, nullable=True)


class UserToNotification(CommonRoutines):
    TYPE_TYPES = [
        (1, u'addcolumn'),
        (2, u'follow'),
        (3, u'unfollow'),
        (4, u'addwall'),
        (5, u'other'),
    ]
    __tablename__ = 'user_to_notification'

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)

    user_to_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=True)
    column_to_id = sa.Column(sa.Integer, sa.ForeignKey('user_column.id'), nullable=True)

    type = sa.Column(ChoiceType(TYPE_TYPES, impl=sa.Integer()), nullable=True)
    action = sa.Column(sa.String, nullable=True)

    msg = sa.Column(sa.String, nullable=True)


"""
class User(Base):
     __tablename__ = 'users'

     id = Column(Integer, primary_key=True)
     name = Column(String)
     fullname = Column(String)
     password = Column(String)

     def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" % (
                             self.name, self.fullname, self.password)
"""

metadata = Base.metadata


def create_all():
    print('create')
    metadata.create_all(engine)
    # user_followers.create(engine)
    # user_following.create(engine)


if __name__ == '__main__':
    create_all()

    # metadata.update
    # create_all()
