from sqlalchemy import desc, asc, text
from sqlalchemy.orm import sessionmaker
from models import engine, User, BaseSite, UserWall, UserColumn, BaseLink, UserColumnLink, UserColumnSettings, \
    UserColumnWallSort

Session = sessionmaker(bind=engine)

session = Session()


def add_user():
    print('yes')
    ed_user = User(username='admin', password='admin', first_name='Andrew', last_name='Andronov',
                   email='89129244869@mail.ru', description='Super user')

    session.add(ed_user)
    session.commit()


def get():
    user = session.query(User).filter(User.username == 'admin').first()
    print('user', user)
    print(user.id)
    session.commit()


def add_base_site():
    ed_user = BaseSite(name='nhl.com', description='NHL.com - The National Hockey League',
                       url='http://www.nhl.com/', rss='http://www.nhl.com/rss/features.xml',
                       image='http://www.nhl.com/images/default/nhl_thumbnail.jpg?v=9.3',
                       favicon='http://cdn.nhle.com/nhl/images/favicon.ico?v=9.3', parent=True)

    session.add(ed_user)
    session.commit()


def add_user_wall():
    ed_user = UserWall(user_id=2, name='Hockey', description='wall of the great hockey', is_active=True,
                       icon='wacher.jpg')

    session.add(ed_user)
    session.commit()


def add_user_column():
    ed_user = UserColumn(user_id=2, wall_id=1, site_id=5, name='nhl.com features',
                         description='The National Hockey League',
                         is_active=True, icon='http://cdn.nhle.com/nhl/images/favicon.ico?v=9.3')

    session.add(ed_user)
    session.commit()


def to_json(user=None, username=None, slugwall=None):
    print('to json')
    data, columns, walls = {}, [], []
    usert = user
    userType = 'Anonymus'
    userTypeColumn = 'Anonymus'
    # userType = 'Alien'
    # userType = 'Current'
    if username and slugwall:
        user = session.query(User).filter(User.username == username).first()
        user_wall = session.query(UserWall).filter(UserWall.is_active == True,  UserWall.user_id == user.id, UserWall.slug == slugwall).order_by(
            desc(UserWall.created)).first()
        if usert:
            if usert == user.id:
                userType = 'Current'
            else:
                userType = 'Alien'
    elif username and not slugwall:
        user = session.query(User).filter(User.username == username).first()
        user_wall = session.query(UserWall).filter(UserWall.is_active == True, UserWall.user_id == user.id, UserWall.home == True).order_by(
            desc(UserWall.created)).first()
        if not user_wall:
            user_wall = session.query(UserWall).filter(UserWall.is_active == True, UserWall.user_id == user.id).order_by(
                desc(UserWall.created)).first()
        if usert:
            if usert == user.id:
                userType = 'Current'
            else:
                userType = 'Alien'
    elif usert:
        user = session.query(User).filter(User.id == usert).first()
        user_wall = session.query(UserWall).filter(UserWall.is_active == True, UserWall.user_id == usert, UserWall.home == True).order_by(
            desc(UserWall.created)).first()
        if not user_wall:
            user_wall = session.query(UserWall).filter(UserWall.is_active == True, UserWall.user_id == usert).order_by(
                desc(UserWall.created)).first()
        if usert:
            if usert == user.id:
                userType = 'Current'
            else:
                userType = 'Alien'
        #return {"none": "nothing", "userType": userType}

    if not user_wall:
        return {"none": "nothing", "userType": userType}

    # user_columns = session.query(UserColumn).filter(UserColumn.user_id == user.id,
    #                                                UserColumn.wall_id == user_wall.id).order_by(desc(UserColumn.created))

    columns_all = session.execute(
        text("SELECT * FROM usercolumn_walls WHERE user_wall_id = " + str(user_wall.id) + ";"))
    columns_ids = [f.usercolumn_wall_id for f in columns_all]
    print(columns_ids)
    user_columns = session.query(UserColumn).filter(UserColumn.user_id == user.id, UserColumn.is_active == True,
                                                    UserColumn.id.in_(columns_ids)).order_by(desc(UserColumn.created))

    for f in session.query(UserWall).filter(UserWall.user_id == user.id).order_by(asc(UserWall.created)):
        current = False
        if f.id == user_wall.id:
            current = True
        walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': current, "color": f.color,
                      "slug": f.slug, "userType": userType, "sort": f.sort, "home": f.home})

    sorter = []

    for f in session.query(UserColumnWallSort).filter(UserColumnWallSort.wall_id == user_wall.id):
        sorter.append({"wall_id": user_wall.id, "column_id": f.column_id, "sort": f.sort})

    print('sorter', sorter)
    # print(filter(lambda n: n.get('column_id') == 21, sorter))

    for column in user_columns:
        if usert == user.id:
            userTypeColumn = 'Current'
        else:
            userTypeColumn = 'Alien'

        cst = session.execute(text("SELECT * FROM usercolumn_walls WHERE usercolumn_wall_id = " + str(column.id) + ";"))
        cws, column_walls = [f.user_wall_id for f in cst], []
        for w in walls:
            if w['id'] in cws:
                column_walls.append(w)

        if column.type == 1:
            items = []
            link_items = session.query(UserColumnLink).filter(UserColumnLink.column_id == column.id,
                                                              UserColumnLink.site == True).order_by(
                desc(UserColumnLink.created)).slice(0, 10)
            st = session.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()

            for it in link_items:
                item = session.query(BaseLink).filter(BaseLink.id == it.link_id).first()
                it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                      "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                      "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail, "json_img": item.json_img,
                      "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                      "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                items.append(it)

            try:
                sort = filter(lambda n: n.get('column_id') == column.id, sorter)[0]['sort']
            except:
                sort = 1

            col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": sort, "items": items,
                   "site_id": column.site_id,
                   "types": column.type, "st": st.as_dict(), "color": column.color, "recontent_items": [],
                   "if_recontent": False, "get_recontent": False,
                   "column_to_rel_column": column.column_to_rel_column, "related_to_items": [],
                   "column_walls": column_walls, "wall_id": user_wall.id, "launch": "standart",
                   "favourites_items": [], "if_favourites": False, "get_favourites": False,
                   "userTypeColumn": userTypeColumn}
            columns.append(col)

        elif column.type == 2:
            items, source_columns = [], []
            link_items = session.query(UserColumnLink).filter(UserColumnLink.column_id == column.id,
                                                              UserColumnLink.word == True).order_by(
                desc(UserColumnLink.created)).slice(0, 10)
            st = session.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()

            cst = session.execute(text("SELECT * FROM column_to_column WHERE column_base_id = " + str(column.id) + ";"))
            cws = [f.column_to_id for f in cst]
            clmns = session.query(UserColumn).filter(UserColumn.user_id == user.id,
                                                     UserColumn.id.in_(cws)).order_by(desc(UserColumn.created))
            for c in clmns:
                source_columns.append({'id': c.id, 'icon': c.icon, 'color': c.color})

            for it in link_items:
                item = session.query(BaseLink).filter(BaseLink.id == it.link_id).first()
                it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                      "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                      "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail, "json_img": item.json_img,
                      "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                      "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                items.append(it)

            try:
                sort = filter(lambda n: n.get('column_id') == column.id, sorter)[0]['sort']
            except:
                sort = 100

            col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": sort, "items": items,
                   "site_id": column.site_id,
                   "types": column.type, "st": st.as_dict(), "color": column.color, "recontent_items": [],
                   "if_recontent": False, "get_recontent": False,
                   "column_to_rel_column": column.column_to_rel_column, "related_to_items": [],
                   "column_walls": column_walls, 'source_columns': source_columns, "wall_id": user_wall.id,
                   "launch": "standart", "favourites_items": [], "if_favourites": False, "get_favourites": False,
                   "userTypeColumn": userTypeColumn}
            columns.append(col)

        elif column.type == 0:
            items = []
            link_items = session.query(UserColumnLink).filter(UserColumnLink.column_id == column.id).order_by(
                desc(UserColumnLink.created)).slice(0, 10)
            st = session.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()

            try:
                color = session.query(BaseSite).filter(BaseSite.id == column.site_id).first().color
            except:
                color = 'red'

            for it in link_items:
                item = session.query(BaseLink).filter(BaseLink.id == it.link_id).first()
                it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                      "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                      "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail, "json_img": item.json_img,
                      "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                      "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                items.append(it)

            sort = filter(lambda n: n.get('column_id') == column.id, sorter)[0]['sort']

            col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": sort, "items": items,
                   "types": column.type, "st": st.as_dict(), "color": color, "recontent_items": [],
                   "if_recontent": False, "get_recontent": False, "wall_id": user_wall.id, "launch": "standart",
                   "favourites_items": [], "if_favourites": False, "get_favourites": False,
                   "userTypeColumn": userTypeColumn}
            columns.append(col)

    data = {"id": user_wall.id, "name": user_wall.name, "color": user_wall.color, "icon": user_wall.icon, "sort": 1,
            'columns': columns,
            'walls': walls, "slug": user_wall.slug, "userType": userType}
    session.commit()
    session.close()
    return data


def to_json_id(id):
    print('to json')
    data, columns, walls = {}, [], []
    user = session.query(User).filter(User.username == 'admin').first()
    user_wall = session.query(UserWall).filter(UserWall.id == id).order_by(desc(UserWall.created)).first()

    # user_columns = session.query(UserColumn).filter(UserColumn.user_id == user.id,
    #                                                UserColumn.wall_id == user_wall.id).order_by(desc(UserColumn.created))

    columns_all = session.execute(text("SELECT * FROM usercolumn_walls WHERE user_wall_id = " + str(id) + ";"))
    columns_ids = [f.usercolumn_wall_id for f in columns_all]
    print(columns_ids)
    user_columns = session.query(UserColumn).filter(UserColumn.user_id == user.id,
                                                    UserColumn.id.in_(columns_ids)).order_by(desc(UserColumn.created))

    for f in session.query(UserWall).filter(UserWall.user_id == user.id).order_by(asc(UserWall.created)):
        current = False
        if f.id == user_wall.id:
            current = True
        walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': current, "color": f.color})

    for column in user_columns:

        cst = session.execute(text("SELECT * FROM usercolumn_walls WHERE usercolumn_wall_id = " + str(column.id) + ";"))
        cws, column_walls = [f.user_wall_id for f in cst], []
        for w in walls:
            if w['id'] in cws:
                column_walls.append(w)

        if column.type == 1:
            items = []
            link_items = session.query(UserColumnLink).filter(UserColumnLink.column_id == column.id,
                                                              UserColumnLink.site == True).order_by(
                desc(UserColumnLink.created)).slice(0, 10)
            st = session.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()
            print('st st st st st', link_items)

            i = 1
            for it in link_items:
                print(i)
                i += 1
                item = session.query(BaseLink).filter(BaseLink.id == it.link_id).first()
                it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                      "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                      "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail, "json_img": item.json_img}
                items.append(it)

            col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": st.sort, "items": items,
                   "site_id": column.site_id,
                   "types": column.type, "st": st.as_dict(), "color": column.color, "recontent_items": [],
                   "if_recontent": False, "get_recontent": False,
                   "column_to_rel_column": column.column_to_rel_column, "related_to_items": [],
                   "column_walls": column_walls}
            columns.append(col)

        elif column.type == 2:
            items = []
            link_items = session.query(UserColumnLink).filter(UserColumnLink.column_id == column.id,
                                                              UserColumnLink.word == True).order_by(
                desc(UserColumnLink.created)).slice(0, 10)
            st = session.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()
            print('st st st st st', link_items)

            i = 1
            for it in link_items:
                print(i)
                i += 1
                item = session.query(BaseLink).filter(BaseLink.id == it.link_id).first()
                it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                      "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                      "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail, "json_img": item.json_img}
                items.append(it)

            col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": st.sort, "items": items,
                   "site_id": column.site_id,
                   "types": column.type, "st": st.as_dict(), "color": column.color, "recontent_items": [],
                   "if_recontent": False, "get_recontent": False,
                   "column_to_rel_column": column.column_to_rel_column, "related_to_items": [],
                   "column_walls": column_walls}
            columns.append(col)

        elif column.type == 0:
            items = []
            link_items = session.query(UserColumnLink).filter(UserColumnLink.column_id == column.id).order_by(
                desc(UserColumnLink.created)).slice(0, 10)
            st = session.query(UserColumnSettings).filter(UserColumnSettings.column_id == column.id).first()

            try:
                color = session.query(BaseSite).filter(BaseSite.id == column.site_id).first().color
            except:
                color = 'red'

            for it in link_items:
                item = session.query(BaseLink).filter(BaseLink.id == it.link_id).first()
                it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                      "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                      "url_path": item.url, "url": item.url}
                items.append(it)

            col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": st.sort, "items": items,
                   "types": column.type, "st": st.as_dict(), "color": color, "recontent_items": [],
                   "if_recontent": False, "get_recontent": False}
            columns.append(col)

    data = {"id": user_wall.id, "name": user_wall.name, "color": user_wall.color, "icon": user_wall.icon, "sort": 1,
            'columns': columns,
            'walls': walls}
    session.commit()
    session.close()
    return data


if __name__ == '__main__':
    # add_user
    # get()
    # add_base_site()
    # add_user_wall()
    # add_user_column()
    # to_json()
    pass
