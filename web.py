#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This example uses Tornado's gen_.
.. _gen: http://www.tornadoweb.org/documentation/gen.html
"""

from __future__ import print_function
import json
import os
import random
import string
import sys
import time
from urlparse import urlparse, parse_qsl

import datetime

from StringIO import StringIO
import psycopg2
import requests
import tweepy
from PIL import Image
from flask import jsonify
from psycopg2.extras import NamedTupleCursor
from requests_oauthlib import OAuth1
from sqlalchemy import text
from tornado.escape import json_encode
import tornado.web
import tornado.ioloop
import tornado.options
from tornado import gen
import tornado.httpserver
from tornado.gen import coroutine
from tornado.websocket import WebSocketHandler
from tornado.wsgi import WSGIContainer
from tornado.web import FallbackHandler, RequestHandler, Application
from tweepy.parsers import JSONParser

import momoko
from admin.config import *
from admin.flasky import authenticateded, w_authenticateded, anonymous_authenticateded, create_token
from related import related_to
from test import to_json, to_json_id
from utils import validate_url, validate_wall_name

db_database = 'contest5'
db_user = 'postgres'
db_password = 'rjierb18'
db_host = 'localhost'
db_port = 5432
enable_hstore = True if os.environ.get('MOMOKO_TEST_HSTORE', False) == '1' else False
dsn = 'dbname=%s user=%s password=%s host=%s port=%s' % (
    db_database, db_user, db_password, db_host, db_port)

assert (db_database or db_user or db_password or db_host or db_port) is not None, (
    'Environment variables for the examples are not set. Please set the following '
    'variables: MOMOKO_TEST_DB, MOMOKO_TEST_USER, MOMOKO_TEST_PASSWORD, '
    'MOMOKO_TEST_HOST, MOMOKO_TEST_PORT')


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db


class BaseHandlerWebSocket(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

    @w_authenticateded
    def get(self, *args, **kwargs):
        if self.current_user:
            return super(BaseHandlerWebSocket, self).get(*args, **kwargs)


class OverviewHandler(BaseHandler):
    def get(self):
        self.write("""
<ul>
    <li><a href="/mogrify">Mogrify</a></li>
    <li><a href="/query">A single query</a></li>
    <li><a href="/hstore">A hstore query</a></li>
    <li><a href="/json">A JSON query</a></li>
    <li><a href="/transaction">A transaction</a></li>
    <li><a href="/multi_query">Multiple queries executed by yielding a list</a></li>
    <li><a href="/connection">Manual connection management</a></li>
</ul>
        """)
        self.finish()


class MogrifyHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        try:
            sql = yield self.db.mogrify("SELECT %s;", (1,))
            self.write("SQL: %s<br>" % sql)
        except Exception as error:
            self.write(str(error))

        self.finish()


class SingleQueryHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        try:
            cursor = yield self.db.execute("SELECT pg_sleep(%s);", (1,))
            self.write("Query results: %s<br>\n" % cursor.fetchall())
        except Exception as error:
            self.write(str(error))

        self.finish()


class HstoreQueryHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        if enable_hstore:
            try:
                cursor = yield self.db.execute("SELECT 'a=>b, c=>d'::hstore;")
                self.write("Query results: %s<br>" % cursor.fetchall())
                cursor = yield self.db.execute("SELECT %s;", ({"e": "f", "g": "h"},))
                self.write("Query results: %s<br>" % cursor.fetchall())
            except Exception as error:
                self.write(str(error))
        else:
            self.write("hstore is not enabled")

        self.finish()


class JsonQueryHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        if self.db.server_version >= 90200:
            try:
                cursor = yield self.db.execute('SELECT \'{"a": "b", "c": "d"}\'::json;')
                self.write("Query results: %s<br>" % cursor.fetchall())
            except Exception as error:
                self.write(str(error))
        else:
            self.write("json is not enabled")

        self.finish()


class MultiQueryHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        cursor1, cursor2, cursor3 = yield [
            self.db.execute("SELECT 1;"),
            self.db.mogrify("SELECT 2;"),
            self.db.execute("SELECT %s;", (3 * 1,))
        ]

        self.write("Query 1 results: %s<br>" % cursor1.fetchall())
        self.write("Query 2 results: %s<br>" % cursor2)
        self.write("Query 3 results: %s" % cursor3.fetchall())

        self.finish()


class TransactionHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        try:
            cursors = yield self.db.transaction((
                "SELECT 1, 12, 22, 11;",
                "SELECT 55, 22, 78, 13;",
                "SELECT 34, 13, 12, 34;",
                "SELECT 23, 12, 22, 23;",
                "SELECT 42, 23, 22, 11;",
                ("SELECT 49, %s, 23, 11;", ("STR",)),
            ))

            for i, cursor in enumerate(cursors):
                self.write("Query %s results: %s<br>" % (i, cursor.fetchall()))
        except Exception as error:
            self.write(str(error))

        self.finish()


class ConnectionQueryHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        self.http_connection_closed = False
        super(ConnectionQueryHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    def get(self):
        start = time.time()

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            now = datetime.datetime.now()
            print(1)
            # WELCOME PAGE
            welcome_walls = [{"id": 1, "columns": [{"id": 45}]}, {"id": 2, "columns": [{"id": 46}]}]
            user = 1  # test
            cursors = yield connection.execute(
                "SELECT * FROM user_wall WHERE id IN (" + ','.join((str(n['id']) for n in welcome_walls)) + "); ")
            w_walls = cursors.fetchall()

            sql = u'INSERT INTO user_wall (is_active, name, icon, color,' \
                  u'slug, sort, home, created, description, user_id) VALUES {} RETURNING id, home;'.format(
                    ', '.join(["(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"] * len(w_walls)))
            params = []
            sort = 1
            for y in w_walls:
                home = True if sort == 1 else False
                params.extend([True, y.name, y.icon, y.color, y.slug, sort, home,
                               datetime.datetime.now(), y.description, user])
                sort += 1

            print('params', params)
            print('sql', sql)
            cursor = yield connection.execute(sql, params)
            # items = []

            bases = cursor.fetchall()
            r_bases = list(reversed(bases))
            print('r_bases', r_bases)
            yi = 0
            for base in r_bases:
                print('base', base.id)

                # IT IS WORKING
                welcome_columns = welcome_walls[yi]["columns"]
                cursor = yield connection.execute(
                        "SELECT * FROM user_column WHERE id IN (" + ','.join(
                                (str(n['id']) for n in welcome_columns)) + ");")
                base_columns = cursor.fetchall()
                print('base_columns', base_columns)

                # CREATE COLUMN
                sql = u"INSERT INTO user_column (is_active, user_id, wall_id, name, type," \
                      u"color, icon, created, site_id, description) VALUES {} " \
                      u"RETURNING id, name, icon, type, color;".format(
                        ', '.join(["(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"] * len(base_columns)))
                params = []
                for bs in base_columns:
                    params.extend([True, user, base.id, bs.name, 1, bs.color, bs.icon, now, bs.site_id,
                                   bs.description])
                print('p create column', params, sql)

                cursor = yield connection.execute(sql, params)
                user_columns = cursor.fetchall()

                # CREATE MODEL TO WALLS
                sql = "INSERT INTO usercolumn_walls (user_wall_id, usercolumn_wall_id) VALUES {} ".format(
                        ', '.join(["(%s, %s)"] * len(user_columns)))
                params = []
                for uc in user_columns:
                    params.extend([base.id, uc.id])
                print('p to walls', params, sql)

                cursor = yield connection.execute(sql, params)

                # CREATE COLUMN SETTINGS
                sql = "INSERT INTO user_column_settings (is_active, user_id, column_id, word_filter, word_exclude, sort, size, type) VALUES {}".format(
                        ', '.join(["(%s, %s, %s, %s, %s, %s, %s, %s)"] * len(user_columns)))
                params = []
                for uc in user_columns:
                    params.extend([True, user, uc.id, '', '', 0, 400, 1])
                print('p settings', params, sql)

                cursor = yield connection.execute(sql, params)

                # CREATE COLUMN SORT
                sql = "INSERT INTO user_column_wall_sort (sort, user_id, column_id, wall_id) VALUES {}".format(
                        ', '.join(["(%s, %s, %s, %s)"] * len(user_columns)))
                params = []
                sort = 1
                for uc in user_columns:
                    params.extend([sort, user, uc.id, base.id])
                    sort += 1
                print('p sort', params, sql)

                cursor = yield connection.execute(sql, params)

                # SELECT BASE LINK
                for column in base_columns:
                    cursor = yield connection.execute(
                            "SELECT * FROM base_link WHERE is_active = {0} AND site_id = {1}  "
                            "ORDER BY created DESC LIMIT 1; ".format(True, column.site_id))  # column.site_id

                    base_links = cursor.fetchall()
                    sql = 'INSERT INTO user_column_link (is_active, user_id, link_id, site_id, ' \
                          'column_id, site, recontent, related, created) VALUES ' \
                          '{};'.format(
                        ', '.join(["(%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s)"] * len(base_links)))
                    params = []
                    for y in base_links:
                        params.extend([True, user, y.id, column.site_id, column.id, True, False, False, y.created])
                        # column.site_id #column
                    print('create item', sql, params)
                    cursor = yield connection.execute(sql, params)

                yi += 1

            """

            cursor = yield connection.execute(
                    "SELECT * FROM base_link WHERE is_active = %s AND site_id = %s  ORDER BY created DESC LIMIT 10; ",
                    (True, site.id,))

            base_links = cursor.fetchall()
            sql = 'INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related, created) VALUES {} RETURNING id, link_id;'.format(', '.join(["(%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s)"] * 10),)
            params = []
            for y in base_links:
                params.extend([True, user, y["id"], site.id, column_id, True, False, False, y["created"]])
            cursor = yield connection.execute(sql, params)
            items = []
            for base in cursor.fetchall():
                pas
            """
            # WELCOME PAGE





            """
            cursor = yield connection.execute(
                                "INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related, created) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}') RETURNING id;".format(
                                        True, user, it.id, site.id, column_id, True, False, False, it.created, ))
                        ite = cursor.fetchone()
            """
            """
            cursor = yield connection.execute(
                            "SELECT * FROM base_link WHERE is_active = %s AND site_id = %s  ORDER BY created DESC LIMIT 10; ",
                            (True, 1,))
            base_links = []
            for base in cursor.fetchall():
                base_links.append({"id": base.id, "title": base.title, "description": base.description,
                                   "tags": base.tags, "small_img": base.small_img, "medium_img": base.medium_img,
                                   "large_img": base.large_img, "url_path": base.url, "url": base.url,
                                   "created": base.created})
            sql = 'INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related, created) VALUES {} RETURNING id, link_id;'.format(', '.join(["(%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s)"] * 10),)
            params = []
            for y in base_links:
                params.extend([True, 1, y["id"], 1, 64, True, False, False, y["created"]])

            cursor = yield connection.execute(sql, params)
            st = time.time()
            items = []
            for base in cursor.fetchall():
                item = filter(lambda n: n.get('id') == base.link_id, base_links)[0]
                items.append({"id": base.id, "title": item["title"], "description": item["description"],
                              "tags": item["tags"], "small_img": item["small_img"],
                              "medium_img": item["medium_img"],
                              "large_img": item["large_img"], "url_path": item["url"], "url": item["url"],
                              "created": item["created"]})
            print('st end', time.time() - st)
            print('items', items)
            """
        print('end', time.time() - start)
        """
        try:
            connection = yield self.db.getconn()
            with self.db.manage(connection):
                for i in range(5):
                    if self.http_connection_closed:
                        break
                    cursor = yield connection.execute("SELECT pg_sleep(1);")
                    self.write('Query %d results: %s<br>\n' % (i + 1, cursor.fetchall()))
                    self.flush()
        except Exception as error:
            self.write(str(error))
        """

        self.finish()

    def on_connection_close(self):
        self.http_connection_closed = True


class UserCreateColumnLetterHandler(BaseHandler):
    """
    Get sites from letter
    """

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            letter = post['letter'].lower()
            types = post['types']

            if letter == 'LUCKY':
                cursor = yield connection.execute(
                    "SELECT * FROM base_site ORDER BY RANDOM() LIMIT 10;")
            elif letter == 'NUMBER':
                cursor = yield connection.execute(
                    "SELECT * FROM base_site WHERE short_url LIKE '" + letter + "%' LIMIT 20;")
            else:
                cursor = yield connection.execute(
                    "SELECT * FROM base_site WHERE short_url LIKE '" + letter + "%' LIMIT 20;")
            """
            if types == 'fromletter':
                cursor = yield connection.execute("SELECT * FROM base_site WHERE short_url LIKE '"+letter+"%' LIMIT 200;")
            else:
                cursor = yield connection.execute("SELECT * FROM base_site WHERE short_url LIKE '%"+letter+"%' LIMIT 50;")
            """
            items = cursor.fetchall()
            print(items)
            if not items:
                self.write({'failure': u'Site not base', "data": []})
            else:
                data = []
                for item in items:
                    print('item', item)
                    print(item.name)
                    data.append({"id": item.id, "name": item.name, "description": item.description,
                                 "rss": item.rss, "favicon": item.favicon, "color": item.color,
                                 "short_url": item.short_url})
                self.write({"success": u'many', "data": data})
                self.finish()


"""
#
# COLUMN
#
"""


class UserCreateColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserCreateColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)
        types = post['types']

        if 'link' not in post and types != 'frommini':
            self.write({'failure': u'Error no link'})
            self.finish()

        connection = yield self.db.getconn()
        start = time.time()

        if types == 'frommini':
            print('frommini')
            print('id', post['id'])  # wall_id

            with self.db.manage(connection):
                cursor = yield connection.execute(
                        "SELECT * FROM user_column WHERE site_id = %s AND user_id = %s AND wall_id = %s",
                        (post['id'], user, post['wall_id'],))
                items = cursor.fetchall()
                print('1', time.time() - start)
                # cursor.close()
                if not items:
                    cursors = yield connection.execute("SELECT * FROM base_site WHERE id = %s", (post['id'],))
                    site = cursors.fetchone()
                    print('2', time.time() - start)

                    now = datetime.datetime.now()

                    cursor = yield connection.execute(
                            "INSERT INTO user_column (is_active, user_id, wall_id, name, type, color, icon, created, site_id) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}') RETURNING id, name, icon, type, color;".format(
                                    True, user, post['wall_id'], site.name, 1, site.color, site.favicon, now,
                                    site.id, ))

                    item = cursor.fetchone()
                    print('3', time.time() - start)
                    # cursor.close()

                    column_id = item.id

                    cur = yield connection.execute(
                            "INSERT INTO usercolumn_walls (user_wall_id, usercolumn_wall_id) VALUES ({0}, {1});".format(
                                    post['wall_id'], item.id, ))
                    cur.close()
                    print('4', time.time() - start)

                    c = yield connection.execute(
                            "INSERT INTO user_column_settings (is_active, user_id, column_id, word_filter, word_exclude, sort, size, type) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id, column_id, word_filter, word_exclude, sort, size, type;".format(
                                    True, user, item.id, '', '', 0,
                                    400, 1))
                    st = c.fetchone()
                    print('5', time.time() - start)
                    c.close()

                    c_sorts = [{'column_id': item.id, 'sort': 1}]
                    cursor = yield connection.execute(
                            "INSERT INTO user_column_wall_sort (sort, user_id, column_id, wall_id) VALUES ({0}, {1}, {2}, {3}) RETURNING id;".format(
                                    1, user, item.id, post['wall_id']))

                    c_sort_c = cursor.fetchone()
                    print('6', time.time() - start)

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_wall_sort WHERE user_id = {0} AND wall_id = {1} ORDER BY sort DESC;".format(
                                    user, post['wall_id'], ))
                    c_sort = cursor.fetchall()
                    print('7', time.time() - start)

                    for f in c_sort:
                        if c_sort_c.id != f.id:
                            cs = f.sort + 1
                            cursor = yield connection.execute(
                                    "UPDATE user_column_wall_sort SET sort = {0} WHERE id = {1}".format(
                                            cs, f.id))
                            c_sorts.append({'column_id': f.column_id, 'sort': cs})

                    """
                    cursor = yield connection.execute(
                            "SELECT * FROM base_link WHERE is_active = %s AND site_id = %s  ORDER BY created DESC LIMIT 10; ",
                            (True, site.id,))
                    base_links = cursor.fetchall()
                    print('8', time.time() - start)
                    print('base_links', base_links)
                    # cursor.close()

                    items = []
                    sql = 'INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related, created) VALUES {} RETURNING id, link_id;'.format(', '.join(["(%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s')"] * 10),)
                    params = []
                    for y in base_links:
                        params.extend([True, user, y.id, site.id, column_id, True, False, False, y.created])
                    for r in range(1, 11):
                        print(r)

                    print('sql', sql)
                    print('parems', params)
                    cursor = yield connection.execute(sql, params)
                    print(cursor.fetchall())
                    """
                    cursor = yield connection.execute(
                            "SELECT * FROM base_link WHERE is_active = %s AND site_id = %s  ORDER BY created DESC LIMIT 10; ",
                            (True, site.id,))
                    base_links = []
                    for base in cursor.fetchall():
                        base_links.append({"id": base.id, "title": base.title, "description": base.description,
                                           "tags": base.tags, "small_img": base.small_img,
                                           "medium_img": base.medium_img,
                                           "large_img": base.large_img, "url_path": base.url, "url": base.url,
                                           "created": base.created, "thumbnail": base.thumbnail,
                                           "json_img": base.json_img,
                                           "if_recontent": False, "if_favourite": False,
                                           "if_recontent_id": None, "if_favourite_id": None})
                    sql = 'INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related, created) VALUES {} RETURNING id, link_id;'.format(
                        ', '.join(["(%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s)"] * 10), )
                    params = []
                    for y in base_links:
                        params.extend([True, user, y["id"], site.id, column_id, True, False, False, y["created"]])

                    cursor = yield connection.execute(sql, params)
                    items = []
                    for base in cursor.fetchall():
                        bs = filter(lambda n: n.get('id') == base.link_id, base_links)[0]
                        items.append({"id": base.id, "title": bs["title"], "description": bs["description"],
                                      "tags": bs["tags"], "small_img": bs["small_img"],
                                      "medium_img": bs["medium_img"],
                                      "large_img": bs["large_img"], "url_path": bs["url"], "url": bs["url"],
                                      "thumbnail": bs["thumbnail"], "json_img": bs["json_img"],
                                      "if_recontent": False, "if_favourite": False,
                                      "if_recontent_id": None, "if_favourite_id": None})
                    """
                    for it in base_links:
                        cursor = yield connection.execute(
                                "INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related, created) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}') RETURNING id;".format(
                                        True, user, it.id, site.id, column_id, True, False, False, it.created, ))
                        ite = cursor.fetchone()
                        # print('item', session.query(UserColumnLink).filter_by(id=it.id).update({"column_id": column.id}))
                        it = {"id": ite.id, "title": it.title, "description": it.description, "tags": it.tags,
                              "small_img": it.small_img, "medium_img": it.medium_img, "large_img": it.large_img,
                              "url_path": it.url, "url": it.url}
                        items.append(it)
                    """
                    print('9', time.time() - start)
                    # cursor.close()
                    # cursor = yield connection.execute("SELECT * FROM user_column_link WHERE is_active = %s AND recontent = %s AND site_id = %s AND user_id IN (" + ','.join((str(n) for n in followings)) + ") ORDER BY created ASC LIMIT 10; ", (True, True, site_id, ))
                    # user_column_links = cursor.fetchall()
                    # print('user_column_links', user_column_links)
                    # #cursor.close()
                    # itemss = list(reversed(items))
                    cursor = yield connection.execute(
                            "SELECT * FROM user_wall WHERE is_active = %s AND user_id = %s  ORDER BY created ASC; ",
                            (True, user,))
                    column_wallss = cursor.fetchall()
                    print('10', time.time() - start)
                    # cursor.close()

                    column_walls = []
                    for f in column_wallss:
                        current = False
                        if f.id == post['wall_id']:
                            current = True
                            column_walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': current,
                                                 "color": f.color, "slug": f.slug, "userType": 'Current'})

                    data = {"id": item.id, "name": item.name, "column_walls": column_walls, "c_sorts": c_sorts,
                            "icon": str(item.icon), "sort": 1, "items": items,
                            "types": item.type, "color": item.color, 'status': 'proccess', 'success': u'create',
                            "st": {"size": st.size, "sort": st.sort, "type": st.type,
                                   "word_exclude": '',
                                   "word_filter": ''}, "recontent_items": [],
                            "if_recontent": False, "get_recontent": False, "wall_id": post['wall_id'],
                            "launch": "standart", "favourites_items": [], "if_favourites": False,
                            "get_favourites": False, "userTypeColumn": "Current"
                            }
                    print('111', time.time() - start)
                    self.write(data)
                    self.finish()
                else:
                    self.write({'failure': u'already have '})
                    self.finish()

            print('all', time.time() - start)


        elif types == 'site':
            link = post['link']
            types = post['types']
            p_l, parse_link = validate_url(link)

            if not p_l:
                print('not validate')
                self.write({'failure': u'Error no link'})
                self.finish()
            else:
                print('validate', parse_link)
                with self.db.manage(connection):
                    short_url = parse_link.netloc
                    cursor = yield connection.execute("SELECT * FROM base_site WHERE short_url = %s", (short_url,))
                    items = cursor.fetchall()
                    print(items)
                    if not items:
                        self.write({'failure': u'Site not base'})
                    else:
                        data = []
                        for item in items:
                            print('item', item)
                            print(item.name)
                            data.append({"id": item.id, "name": item.name, "description": item.description,
                                         "rss": item.rss, "favicon": item.favicon, "color": item.color,
                                         "short_url": item.short_url})
                        self.write({"success": u'many', "data": data})
                        self.finish()
                        # else:
                        # self.write({'success': u'one'})
                    print('ffff')
        elif types == 'collection':
            link = post['link']

            print('collection', post)
            if len(post['name']) == 0:
                self.write({'failure': u'Not name'})
                self.finish()
            elif len(post['name']) > 20:
                self.write({'failure': u'Max length'})
                self.finish()
            elif 'sources' not in post or 'walls' not in post:
                self.write({'failure': u'blank'})
                self.finish()
            else:
                with self.db.manage(connection):
                    cursor = yield connection.execute("SELECT * FROM user_column WHERE name = %s AND type = %s",
                                                      (post['name'], 2,))
                    items = cursor.fetchall()
                    # cursor.close()
                    if len(items) >= 30:
                        self.write({'failure': u'too many column'})
                        self.finish()
                    else:
                        cursor = yield connection.execute(
                                u"INSERT INTO user_column (is_active, user_id, wall_id, name, type, color, icon) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}') RETURNING id, name, icon, type, color, wall_id;".format(
                                        True, user, post['wall_id'], post['name'], 2, post['color'], ''))

                        item = cursor.fetchone()

                        cur = yield connection.execute(
                                "INSERT INTO usercolumn_walls (user_wall_id, usercolumn_wall_id) VALUES ({0}, {1});".format(
                                        item.wall_id, item.id, ))

                        c = yield connection.execute(
                                u"INSERT INTO user_column_settings (is_active, user_id, column_id, word_filter, word_exclude, sort, size, type) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id, column_id, word_filter, word_exclude, sort, size, type;".format(
                                        True, user, item.id, ', '.join(post['matchings']),
                                        ', '.join(post['excludings']), 0,
                                        400, 1))
                        st = c.fetchone()
                        print(c, item)

                        cursor = yield connection.execute(
                                "SELECT * FROM user_wall WHERE is_active = %s AND user_id = %s  ORDER BY sort DESC; ",
                                (True, user,))
                        column_wallss = cursor.fetchall()
                        # cursor.close()

                        column_walls = []
                        for f in column_wallss:
                            current = False
                            if f.id == post['wall_id']:
                                current = True
                                # if f.id in post["walls"]:
                                column_walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': current,
                                                     "color": f.color, "slug": f.slug, "userType": 'Current',
                                                     "sort": f.sort})

                        c_sorts = [{'column_id': item.id, 'sort': 1}]
                        cursor = yield connection.execute(
                                "INSERT INTO user_column_wall_sort (sort, user_id, column_id, wall_id) VALUES ({0}, {1}, {2}, {3}) RETURNING id;".format(
                                        1, user, item.id, post['wall_id']))

                        c_sort_c = cursor.fetchone()

                        cursor = yield connection.execute(
                                "SELECT * FROM user_column_wall_sort WHERE user_id = {0} AND wall_id = {1} ORDER BY sort DESC;".format(
                                        user, post['wall_id'], ))
                        c_sort = cursor.fetchall()

                        for f in c_sort:
                            if c_sort_c.id != f.id:
                                cs = f.sort + 1
                                cursor = yield connection.execute(
                                        "UPDATE user_column_wall_sort SET sort = {0} WHERE id = {1}".format(
                                                cs, f.id))
                                c_sorts.append({'column_id': f.column_id, 'sort': cs})
                        """
                        for wall in post["walls"]:
                            if wall != post['wall_id']:
                                cursors = yield connection.execute(
                                "INSERT INTO user_column_wall_sort (sort, user_id, column_id, wall_id) VALUES ({0}, {1}, {2}, {3})".format(
                                s['sort'], user, item.id, wall))
                                cursors.close()
                        """
                        """
                        sorts = []

                        for wall in post["walls"]:
                            if wall != post['wall_id']:
                                cursors = yield connection.execute(
                                        "SELECT * FROM user_column_wall_sort WHERE user_id = {0} AND wall_id = {1}".format(
                                                user, wall, ))
                                f = cursors.fetchall()

                                for s in f:
                                    sorts.append(s.sort)
                        """

                        source_columns = []

                        for c in post['sources']:
                            cursors = yield connection.execute(
                                    "INSERT INTO column_to_column (column_base_id, column_to_id) VALUES ({0}, {1});".format(
                                            item.id, c))
                            cursors.close()

                        print('p', ','.join((str(n) for n in post['sources'])))
                        cursors = yield connection.execute("SELECT * FROM user_column WHERE id IN (" + ','.join(
                                (str(n) for n in post['sources'])) + "); ")
                        clmns = cursors.fetchall()
                        for c in clmns:
                            source_columns.append({'id': c.id, 'icon': c.icon, 'color': c.color})

                        try:
                            word_filter = st.word_filter.split(', ')
                        except:
                            word_filter = []

                        try:
                            word_exclude = st.word_exclude.split(', ')
                        except:
                            word_exclude = []

                        data = {"id": item.id, "name": item.name, "c_sorts": c_sorts,
                                "icon": '', "sort": 1, "items": [],
                                "types": item.type, "color": item.color, 'status': 'proccess', 'success': u'create',
                                "st": {"size": st.size, "sort": 1, "type": st.type,
                                       "word_exclude": word_exclude,
                                       "word_filter": word_filter},
                                "column_walls": column_walls, "recontent_items": [],
                                "if_recontent": False, "get_recontent": False, "wall_id": post['wall_id'],
                                "launch": "standart", "favourites_items": [], "if_favourites": False,
                                "get_favourites": False, "source_columns": source_columns,
                                "userTypeColumn": "Current"}
                        self.write(data)
                        self.finish()
                        # self.write({'success': u'create'})


"""
#
# COLUMN SIZE SETTINGS
#
"""


class UserChangeSizeColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeSizeColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'size' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        size = post['size']
        print(post)

        connection = yield self.db.getconn()

        # exists
        if size not in [200, 300, 400, 500, 600]:
            self.write({'failure': u'No current size'})
        else:
            with self.db.manage(connection):
                cursor = yield connection.execute(
                        "SELECT id, user_id FROM user_column WHERE user_id = {0} AND id = {1} ".format(user, column))
                f = cursor.fetchall()
                print(f)
                if not f:
                    self.write({'failure': u'No column'})
                else:
                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_settings WHERE user_id = {0} AND column_id = {1} ".format(user,
                                                                                                                 column))
                    settings = cursor.fetchone()
                    print('settings', settings)
                    if settings:
                        cursor = yield connection.execute(
                                "UPDATE user_column_settings SET size = {2} WHERE user_id = {0} AND column_id = {1}".format(
                                        user, column, size))
                        self.write({'success': u'update success'})
                    else:
                        self.write({'failure': u'No settings'})

        self.finish()

        # self.write({'success': u'create column'})


"""
#
# COLUMN NOTI REALTIME SETTINGS
#
"""


class UserChangeRealtimeColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeRealtimeColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        if 'action' not in post and 'column' not in post:
            self.write({'failure': u'no data'})

        column = post['column']
        action = post['action']
        print(post)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                    "SELECT id, user_id FROM user_column WHERE user_id = {0} AND id = {1} ".format(user, column))
            f = cursor.fetchall()
            # cursor.close()
            print(f)
            if not f:
                self.write({'failure': u'No column'})
            else:
                cursor = yield connection.execute(
                        "SELECT * FROM user_column_settings WHERE user_id = {0} AND column_id = {1} ".format(user,
                                                                                                             column))
                settings = cursor.fetchone()
                # cursor.close()
                print('settings', settings)
                if settings:

                    act = True
                    if action == u'No':
                        act = False

                    cursor = yield connection.execute(
                            "UPDATE user_column_settings SET realtime = {2} WHERE user_id = {0} AND column_id = {1}".format(
                                    user, column, act))
                    # cursor.close()
                    self.write({'success': u'update success'})
                else:
                    self.write({'failure': u'No settings'})

        self.finish()


"""
#
# COLUMN TOP WALL TOP
#
"""


class UserTopColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserTopColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post)

        if user:
            connection = yield self.db.getconn()

            with self.db.manage(connection):
                wall_id = post['wall_id']
                now = datetime.datetime.now()
                past = now - datetime.timedelta(days=1)
                psd = datetime.datetime(past.year, past.month, past.day)
                items = []

                cursor = yield connection.execute(
                        "SELECT * FROM usercolumn_walls WHERE user_wall_id = {0};".format(wall_id))
                columns_ids = [f.usercolumn_wall_id for f in cursor.fetchall()]
                print('columns_ids', columns_ids)

                cursor = yield connection.execute(
                        "SELECT * FROM user_column WHERE is_active = TRUE AND id IN (" + ','.join(
                                (str(n) for n in columns_ids)) + "); ")
                site_ids = [s.site_id for s in cursor.fetchall()]
                print('site_ids', site_ids)

                cursor = yield connection.execute(
                        "SELECT * FROM base_link WHERE is_active = TRUE AND site_id IN (" + ','.join((str(n) for n in
                                                                                                      site_ids)) + ") AND created <= '{0}' ORDER BY counter DESC LIMIT 20; ".format(
                            psd))
                links = cursor.fetchall()

                for ite in links:
                    print('is', ite)
                    it = {"id": ite.id, "title": ite.title, "description": ite.description, "tags": ite.tags,
                          "small_img": ite.small_img, "medium_img": ite.medium_img, "large_img": ite.large_img,
                          "url_path": ite.url, "url": ite.url, "thumbnail": ite.thumbnail, "json_img": ite.json_img,
                          "site_id": ite.site_id}
                    print('is', it)
                    items.append(it)

                self.write({'success': u'just do it', 'data': items})

        self.finish()


"""
#
# COLUMN SORT SETTINGS
#
"""


class UserChangeSortColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeSortColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        print(user, post)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            for s in post['sorter']:
                print(s)
                cursor = yield connection.execute(
                        "SELECT * FROM user_column_wall_sort WHERE user_id = {0} AND column_id = {1} AND wall_id = {2}".format(
                                user, s['column'], s['wall_id']))
                f = cursor.fetchall()
                if f:
                    cursor = yield connection.execute(
                            "UPDATE user_column_wall_sort SET sort = {0} WHERE user_id = {1} AND column_id = {2} AND wall_id = {3}".format(
                                    s['sort'], user, s['column'], s['wall_id']))
                    # cursor.close()
                else:
                    cursor = yield connection.execute(
                            "INSERT INTO user_column_wall_sort (sort, user_id, column_id, wall_id) VALUES ({0}, {1}, {2}, {3})".format(
                                    s['sort'], user, s['column'], s['wall_id']))
                    # cursor.close()

            self.write({'success': u'success'})
            """
                cursor = yield connection.execute(
                    "SELECT id, user_id FROM user_column WHERE user_id = {0} AND id = {1} ".format(user, column))
                f = cursor.fetchall()
                print(f)
                if not f:
                    self.write({'failure': u'No column'})
                else:
                    cursor = yield connection.execute(
                        "SELECT * FROM user_column_settings WHERE user_id = {0} AND column_id = {1} ".format(user,
                                                                                                             column))
                    settings = cursor.fetchone()
                    print('settings', settings)
                    if settings:
                        cursor = yield connection.execute(
                            "UPDATE user_column_settings SET size = {2} WHERE user_id = {0} AND column_id = {1}".format(
                                user, column, size))
                        self.write({'success': u'update success'})
                    else:
                        self.write({'failure': u'No settings'})

                        # self.write({'success': u'create column'})
            """

        self.finish()


"""
#
# COLUMN DELETE SETTINGS
#
"""


class UserDeleteColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserDeleteColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        print(user, post)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            column_id = post['column_id']
            cursor = yield connection.execute(
                    "SELECT id, user_id FROM user_column WHERE user_id = {0} AND id = {1} ".format(user, column_id))
            f = cursor.fetchall()
            # cursor.close()
            print(f)
            if not f:
                self.write({'failure': u'No column'})
            else:
                cursor = yield connection.execute(
                        "UPDATE user_column SET is_active = '{0}' WHERE user_id = {1} AND id = {2};".format(
                                False, user, column_id))

            self.write({'success': u'success'})

        self.finish()


"""
#
# COLUMN SORT SETTINGS
#
"""


class UserChangeWordsColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeWordsColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        print(user, post)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            if post['action'] == u'matching':
                w = ', '.join(post['words'])
                cursor = yield connection.execute(
                        u"UPDATE user_column_settings SET word_filter = '{0}' WHERE column_id = {1};".format(
                                w, post['column']))
                # cursor.close()
            elif post['action'] == u'excluding':
                w = ', '.join(post['words'])
                cursor = yield connection.execute(
                        u"UPDATE user_column_settings SET word_exclude = '{0}' WHERE column_id = {1};".format(
                                w, post['column']))
                # cursor.close()

            self.write({'success': u'success'})
            """
                cursor = yield connection.execute(
                    "SELECT id, user_id FROM user_column WHERE user_id = {0} AND id = {1} ".format(user, column))
                f = cursor.fetchall()
                print(f)
                if not f:
                    self.write({'failure': u'No column'})
                else:
                    cursor = yield connection.execute(
                        "SELECT * FROM user_column_settings WHERE user_id = {0} AND column_id = {1} ".format(user,
                                                                                                             column))
                    settings = cursor.fetchone()
                    print('settings', settings)
                    if settings:
                        cursor = yield connection.execute(
                            "UPDATE user_column_settings SET size = {2} WHERE user_id = {0} AND column_id = {1}".format(
                                user, column, size))
                        self.write({'success': u'update success'})
                    else:
                        self.write({'failure': u'No settings'})

                        # self.write({'success': u'create column'})
            """
        self.finish()


"""
#
# COLUMN WORD SOURCE SETTINGS
#
"""


class UserChangeSourceWColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeSourceWColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        # self.render("templates/column/get_list_source_w.html", items=['dd', 'dtr'])
        # print(w)
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        pass

        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute(
                    "SELECT * FROM user_column WHERE user_id = %s AND type = %s AND is_active = %s", (user, 1, True,))
            items = cursor.fetchall()
            print(items)
            if not items:
                self.write({'failure': u'Nothing'})
            elif len(items) > 0:
                data = []
                for item in items:
                    c = yield connection.execute("SELECT * FROM base_site WHERE id = %s", (item.site_id,))
                    site = c.fetchone()
                    print('item', item)
                    print(item.name)
                    data.append({"id": item.id, "name": item.name, "description": item.description,
                                 "icon": item.icon, "color": site.color})
                self.write({"success": u'success', "data": data})

        self.finish()


"""
#
# COLUMN SOURCE SETTINGS
#
"""


class UserChangeSourceEditWColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeSourceEditWColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        # self.render("templates/column/get_list_source_w.html", items=['dd', 'dtr'])
        # print(w)
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):

        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserChangeSourceEditWColumnHandler', post)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            column_id = post['column']
            action = post['action']
            new_id = post['id']
            data = []
            if action == 'add':
                cursors = yield connection.execute("SELECT * FROM user_column WHERE id = %s AND user_id = %s",
                                                   (column_id, user))
                result = cursors.fetchall()
                cursors.close()
                if result:
                    cur = yield connection.execute(
                            "INSERT INTO column_to_column (column_base_id, column_to_id) VALUES ({0}, {1});".format(
                                    column_id, new_id, ))
                    cur.close()

                    c = yield connection.execute("SELECT * FROM column_to_column WHERE column_base_id = %s; ",
                                                 (column_id,))
                    columns_ids = [f.column_to_id for f in c.fetchall()]
                    print('columns_ids', columns_ids)
                    c.close()

                    print("" + ','.join((str(n) for n in columns_ids)) + "")

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column WHERE id IN (" + ','.join((str(n) for n in columns_ids)) + "); ",
                            ())
                    clmns = cursor.fetchall()
                    # cursor.close()
                    for c in clmns:
                        data.append({'id': c.id, 'icon': c.icon, 'color': c.color})
            else:
                cursors = yield connection.execute("SELECT * FROM user_column WHERE id = %s AND user_id = %s",
                                                   (column_id, user))
                result = cursors.fetchall()
                cursors.close()
                if result:
                    cur = yield connection.execute(
                            "DELETE FROM  column_to_column WHERE column_base_id = {0} AND column_to_id = {1};".format(
                                    column_id, new_id, ))
                    cur.close()

                    c = yield connection.execute("SELECT * FROM column_to_column WHERE column_base_id = %s; ",
                                                 (column_id,))
                    columns_ids = [f.column_to_id for f in c.fetchall()]
                    print('columns_ids', columns_ids)
                    c.close()

                    print("" + ','.join((str(n) for n in columns_ids)) + "")

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column WHERE id IN (" + ','.join((str(n) for n in columns_ids)) + "); ",
                            ())
                    clmns = cursor.fetchall()
                    # cursor.close()
                    for c in clmns:
                        data.append({'id': c.id, 'icon': c.icon, 'color': c.color})

            self.write({"success": u'success', "data": data})

        self.finish()


"""
#
# COLUMN PRE Loader
#
"""


class UserPreloaderColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserPreloaderColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)
        if post['action'] == u'standart':
            print('yes')
            data, items = {}, []
            ids = post['ids']
            page = post['page']
            connection = yield self.db.getconn()
            with self.db.manage(connection):
                cursor = yield connection.execute(
                        "SELECT * FROM user_column_link WHERE is_active = %s AND column_id = %s AND id NOT IN (" + ','.join(
                                (str(n) for n in ids)) + ") ORDER BY created DESC LIMIT 10; ", (True, post['column'],))
                links = cursor.fetchall()
                # cursor.close()
                print('links', links)
                data['page'] = page + 10

                for it in links:
                    cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s", (it.link_id,))
                    ite = cursor.fetchone()
                    it = {"id": it.id, "title": ite.title, "description": ite.description, "tags": ite.tags,
                          "small_img": ite.small_img, "medium_img": ite.medium_img, "large_img": ite.large_img,
                          "url_path": ite.url, "url": ite.url, "thumbnail": ite.thumbnail, "json_img": ite.json_img,
                          "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                          "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                    items.append(it)
                    # cursor.close()

                data['items'] = items

                self.write({"success": u'success', "data": data})

        self.finish()


"""
#
# COLUMN WORD WALLS SETTINGS
#
"""


class UserChangeWallsWColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeWallsWColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        # self.render("templates/column/get_list_source_w.html", items=['dd', 'dtr'])
        # print(w)
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        pass

        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute("SELECT * FROM user_wall WHERE user_id = %s AND is_active = %s",
                                              (user, True,))
            items = cursor.fetchall()
            print(items)
            if not items:
                self.write({'failure': u'Nothing'})
            elif len(items) > 0:
                data = []
                for item in items:
                    print('item', item)
                    print(item.name)
                    data.append({"id": item.id, "name": item.name, "description": item.description,
                                 "icon": item.icon, "color": item.color})
                self.write({"success": u'success', "data": data})

        self.finish()


"""
#
# COLUMN GET WALLS SETTINGS
#
"""


class UserChangeWallsEditWColumnHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserChangeWallsEditWColumnHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        # self.render("templates/column/get_list_source_w.html", items=['dd', 'dtr'])
        # print(w)
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        pass

        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserChangeWallsEditWColumnHandler', post)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            wall_id = post['id']
            column_id = post['column']
            action = post['action']
            current_id = post['current_id']
            data = []
            if action == 'add':
                cursors = yield connection.execute("SELECT * FROM user_wall WHERE id = %s AND user_id = %s",
                                                   (wall_id, user))
                result = cursors.fetchall()
                cursors.close()
                if result:
                    cur = yield connection.execute(
                            "INSERT INTO usercolumn_walls (user_wall_id, usercolumn_wall_id) VALUES ({0}, {1});".format(
                                    wall_id, column_id, ))
                    cur.close()

                    c = yield connection.execute("SELECT * FROM usercolumn_walls WHERE usercolumn_wall_id = %s; ",
                                                 (column_id,))
                    walls_ids = [f.user_wall_id for f in c.fetchall()]
                    print('walls_ids', walls_ids)
                    c.close()

                    print("" + ','.join((str(n) for n in walls_ids)) + "")

                    cursor = yield connection.execute(
                            "SELECT * FROM user_wall WHERE id IN (" + ','.join((str(n) for n in walls_ids)) + "); ", ())
                    walls = cursor.fetchall()
                    # cursor.close()
                    for w in walls:
                        current = False
                        if w.id == current_id:
                            current = True
                        data.append({'id': w.id, 'name': w.name, 'icon': w.icon, 'current': current, "color": w.color})
            else:
                cursors = yield connection.execute("SELECT * FROM user_wall WHERE id = %s AND user_id = %s",
                                                   (wall_id, user))
                result = cursors.fetchall()
                cursors.close()
                if result:
                    cur = yield connection.execute(
                            "DELETE FROM usercolumn_walls WHERE user_wall_id = {0} AND usercolumn_wall_id = {1};".format(
                                    wall_id, column_id, ))
                    cur.close()

                    c = yield connection.execute("SELECT * FROM usercolumn_walls WHERE usercolumn_wall_id = %s; ",
                                                 (column_id,))
                    walls_ids = [f.user_wall_id for f in c.fetchall()]
                    print('walls_ids', walls_ids)
                    c.close()

                    print("" + ','.join((str(n) for n in walls_ids)) + "")

                    cursor = yield connection.execute(
                            "SELECT * FROM user_wall WHERE id IN (" + ','.join((str(n) for n in walls_ids)) + "); ", ())
                    walls = cursor.fetchall()
                    # cursor.close()
                    for w in walls:
                        current = False
                        if w.id == current_id:
                            current = True
                        data.append({'id': w.id, 'name': w.name, 'icon': w.icon, 'current': current, "color": w.color})

            self.write({"success": u'success', "data": data})

        self.finish()


"""
#
# COLUMN Favourite save
#
"""


class UserFavouriteHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserFavouriteHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        connection = yield self.db.getconn()

        if 'id' not in post:
            self.write({'failure': u'no id'})
        else:
            if post['action'] == u'add':
                with self.db.manage(connection):
                    cursor = yield connection.execute("SELECT * FROM user_column_link WHERE id = %s AND user_id = %s; ",
                                                      (post["id"], user,))
                    link = cursor.fetchone()
                    # cursor.close()
                    if link:
                        curso = yield connection.execute(
                                "UPDATE user_column_link SET if_favourite = {0} WHERE id = {1};".format(True,
                                                                                                        post["id"]))
                        curso.close()
                        self.write({'success': u'success', 'action': u'add'})
            elif post['action'] == u'remove':
                with self.db.manage(connection):
                    cursor = yield connection.execute("SELECT * FROM user_column_link WHERE id = %s AND user_id = %s; ",
                                                      (post["id"], user,))
                    link = cursor.fetchone()
                    # cursor.close()
                    if link:
                        curso = yield connection.execute(
                                "UPDATE user_column_link SET if_favourite = {0} WHERE id = {1};".format(False,
                                                                                                        post["id"]))
                        curso.close()
                        self.write({'success': u'success', 'action': u'remove'})

        self.finish()


"""
#
# COLUMN ReContent
#
"""


class UserReContentHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserReContentHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        connection = yield self.db.getconn()

        if 'id' not in post:
            self.write({'failure': u'no id'})
        else:
            if post['action'] == u'add':
                print('yess')
                with self.db.manage(connection):
                    cursor = yield connection.execute("SELECT * FROM user_column_link WHERE id = %s AND user_id = %s; ",
                                                      (post["id"], user,))
                    link = cursor.fetchone()
                    # cursor.close()

                    c = yield connection.execute(
                            "SELECT * FROM user_column_link WHERE user_id = %s AND link_id = %s AND site_id = %s AND column_id = %s AND recontent = %s; ",
                            (user, link.link_id, link.site_id, link.column_id, True,))
                    links = c.fetchall()
                    print('links', links)
                    c.close()

                    if not links:

                        cs = yield connection.execute(
                                "INSERT INTO user_column_link (is_active, user_id, link_id, site_id, column_id, site, recontent, related) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id;".format(
                                        True, user, link.link_id, link.site_id, link.column_id, False, True, False, ))
                        ct = cs.fetchone()
                        print('ct', ct)
                        cs.close()

                        curso = yield connection.execute(
                                "UPDATE user_column_link SET if_recontent = {0} , if_recontent_id = {1} WHERE id = {2};".format(
                                        True, ct.id, post["id"]))
                        curso.close()

                        self.write({'success': u'success', 'action': u'add', 'column_id': ct.id})

                        cursor = yield connection.execute("SELECT * FROM user_followers WHERE user_base_id = %s",
                                                          (user,))
                        followers = cursor.fetchall()
                        # cursor.close()

                        if followers:
                            followers = [f.user_to_id for f in followers]
                            # cursor = yield connection.execute("SELECT * FROM user_column_link WHERE is_active = %s AND recontent = %s AND site_id = %s AND user_id IN (" + ','.join((str(n) for n in followings)) + ") ORDER BY created ASC LIMIT 10; ", (True, True, site_id, ))
                            for f in followers:
                                s = yield connection.execute(
                                        "INSERT INTO user_column_link (is_active, user_id, link_id, site_id, site, recontent, related) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}') RETURNING id;".format(
                                                True, f, link.link_id, link.site_id, False, True, False, ))
                                el = s.fetchone()
                                s.close()

                                data = {"id": el.id, "site_id": link.site_id, "link_id": link.link_id, "wall_id": 1,
                                        "column_id": 1, "post": {}}
                                cmd = json.dumps(
                                        {"cmd": "INSERT", "type": "USER", "user": f, "name": "ws_new_related_links",
                                         "data": data})
                                notify = "NOTIFY users, '{0}'".format(str(cmd), )
                                cs = yield connection.execute(notify)
                                cs.close()
                            print('followers', followers)
                    else:
                        curso = yield connection.execute(
                                "UPDATE user_column_link SET if_recontent = {0} , if_recontent_id = NULL WHERE id = {1};".format(
                                        False, post["id"]))
                        curso.close()

                        c = yield connection.execute(
                                "DELETE FROM  user_column_link WHERE user_id = %s AND link_id = %s AND site_id = %s AND column_id = %s AND recontent = %s; ",
                                (user, link.link_id, link.site_id, link.column_id, True,))
                        c.close()
                        self.write({'success': u'success', 'action': u'remove'})

                        cursor = yield connection.execute("SELECT * FROM user_followers WHERE user_base_id = %s",
                                                          (user,))
                        followers = cursor.fetchall()
                        # cursor.close()

                        if followers:
                            followers = [f.user_to_id for f in followers]
                            # cursor = yield connection.execute("SELECT * FROM user_column_link WHERE is_active = %s AND recontent = %s AND site_id = %s AND user_id IN (" + ','.join((str(n) for n in followings)) + ") ORDER BY created ASC LIMIT 10; ", (True, True, site_id, ))
                            for f in followers:
                                s = yield connection.execute(
                                        "DELETE FROM user_column_link WHERE is_active = {0} AND user_id = {1} AND link_id = {2} AND site_id = {3} AND site = {4} AND recontent = {5} AND related = {6};".format(
                                                True, f, link.link_id, link.site_id, False, True, False, ))
                                s.close()

                            print('followers two', followers)

        self.finish()


"""
#
# COLUMN ReContent
#
"""


class UserGetReContentHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserGetReContentHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'column' not in post:
            self.write({'failure': u'Error no column'})
        else:
            connection = yield self.db.getconn()

            with self.db.manage(connection):
                cursor = yield connection.execute("SELECT * FROM user_following WHERE user_base_id = %s", (user,))
                following = cursor.fetchall()
                # cursor.close()

                if not following:
                    self.write({'failure': u'none', 'get_recontent': True, 'if_recontent': False,
                                'recontent_items': []})
                else:
                    followings = [f.user_to_id for f in following]
                    print('followings', followings)
                    cursor = yield connection.execute("SELECT * FROM user_column WHERE id = %s", (post['column'],))
                    column = cursor.fetchone()
                    # cursor.close()
                    site_id = column.site_id

                    print('site_id', site_id)
                    print("" + ','.join((str(n) for n in followings)) + "")

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_link WHERE is_active = %s AND recontent = %s AND site_id = %s AND user_id IN (" + ','.join(
                                    (str(n) for n in followings)) + ") ORDER BY created ASC LIMIT 10; ",
                            (True, True, site_id,))
                    user_column_links = cursor.fetchall()
                    print('user_column_links', user_column_links)
                    # cursor.close()

                    items = []
                    for it in user_column_links:
                        cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s", (it.link_id,))
                        item = cursor.fetchone()
                        # print('item', session.query(UserColumnLink).filter_by(id=it.id).update({"column_id": column.id}))
                        its = {"id": it.id, "title": item.title, "description": item.description,
                               "tags": item.tags,
                               "small_img": item.small_img, "medium_img": item.medium_img,
                               "large_img": item.large_img,
                               "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail,
                               "json_img": item.json_img,
                               "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                               "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                        items.append(its)
                        # cursor.close()

                    self.write({'success': u'data', 'get_recontent': True, 'if_recontent': True,
                                'recontent_items': items})

        self.finish()


"""
#
# COLUMN GET FAVOURITE
#
"""


class UserGetFavouriteHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserGetFavouriteHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'column' not in post:
            self.write({'failure': u'Error no column'})
        else:
            connection = yield self.db.getconn()

            with self.db.manage(connection):
                cursor = yield connection.execute("SELECT * FROM user_column WHERE id = %s AND user_id = %s",
                                                  (post['column'], user,))
                column = cursor.fetchone()
                # cursor.close()

                if not column:
                    self.write({'failure': u'none', 'get_favourites': True, 'if_favourites': False,
                                'favourites_items': []})
                else:

                    column_id = column.id

                    print('column_id', column_id)

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_link WHERE is_active = %s AND if_favourite = %s AND column_id = %s AND user_id = %s ORDER BY created ASC LIMIT 10; ",
                            (True, True, column_id, user,))
                    user_column_links = cursor.fetchall()
                    print('user_column_links', user_column_links)
                    # cursor.close()

                    items = []
                    for it in user_column_links:
                        cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s", (it.link_id,))
                        item = cursor.fetchone()
                        # print('item', session.query(UserColumnLink).filter_by(id=it.id).update({"column_id": column.id}))
                        its = {"id": it.id, "title": item.title, "description": item.description,
                               "tags": item.tags,
                               "small_img": item.small_img, "medium_img": item.medium_img,
                               "large_img": item.large_img,
                               "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail,
                               "json_img": item.json_img,
                               "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                               "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                        items.append(its)
                        # cursor.close()

                    self.write({'success': u'data', 'get_favourites': True, 'if_favourites': True,
                                'favourites_items': items})

        self.finish()


"""
#
# COLUMN GET RELATED
#
"""


class UserGetRelatedHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserGetRelatedHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'id' not in post:
            self.write({'failure': u'Error no link'})

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            data = {}
            cursor = yield connection.execute("SELECT * FROM user_column_link WHERE id = %s AND user_id = %s;",
                                              (post['id'], user,))
            user_link = cursor.fetchone()
            # cursor.close()
            print('user_link', user_link)

            cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s;", (user_link.link_id,))
            link = cursor.fetchone()
            # cursor.close()
            print('link', link)

            cursor = yield connection.execute(
                    "SELECT * FROM base_link WHERE is_active = %s AND id != %s ORDER BY created ASC LIMIT 5000;",
                    (True, link.id,))
            links = cursor.fetchall()
            result = related_to(link, links)
            print('res', result)

            cursor = yield connection.execute(
                    "SELECT * FROM base_link WHERE id IN (" + ','.join((str(n['id']) for n in result)) + ");")
            items = cursor.fetchall()
            print('items', items)
            result_items = []

            for item in items:
                print('it', item)
                # cs = yield connection.execute(
                #        "INSERT INTO user_column_link (is_active, user_id, site_id, link_id, column_id,  site, related, related_children) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id, is_active, user_id, site_id, link_id, column_id,  site, related, related_children;".format(
                #            True, user, user_link.site_id, it.id, item.id, False, True, user_link.id,))
                # ct = cs.fetchone()
                i = {"id": item.id, "title": item.title, "description": item.description,
                     "tags": item.tags,
                     "small_img": item.small_img, "medium_img": item.medium_img,
                     "large_img": item.large_img,
                     "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail,
                     "json_img": item.json_img}
                result_items.append(i)
                # cs.close()

            data["items"] = result_items
            self.write({"success": u'success', "result": "top5", "data": data})

        self.finish()


"""
#
# COLUMN Related Create
#
"""


class UserRelatedCreateHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserRelatedCreateHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'id' not in post:
            self.write({'failure': u'Error no link'})

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute("SELECT * FROM user_column_link WHERE id = %s AND user_id = %s;",
                                              (post['id'], user,))
            user_link = cursor.fetchone()
            # cursor.close()
            print('user_link', user_link)

            cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s;", (user_link.link_id,))
            link = cursor.fetchone()
            # cursor.close()
            print('link', link)

            cursor = yield connection.execute(
                    "SELECT * FROM base_link WHERE is_active = %s AND id != %s ORDER BY created DESC LIMIT 100;",
                    (True, link.id,))
            links = cursor.fetchall()
            result = related_to(link, links)
            print('res', result)

            """
            column_to_rel_column = None

            cursor = yield connection.execute("SELECT * FROM user_column_link WHERE id = %s AND user_id = %s;", (post['id'], user, ))
            user_link = cursor.fetchone()
            print(user_link)
            cursor = yield connection.execute("SELECT * FROM user_column WHERE id = %s;", (user_link.column_id, ))
            column = cursor.fetchone()
            if column.column_to_rel_column:
                column_to_rel_column = column.column_to_rel_column
            cursor = yield connection.execute("SELECT * FROM user_column WHERE related_link_id = %s AND type = %s;", (user_link.id, 4, ))
            user_column = cursor.fetchall()
            print(user_column)
            if not user_column:
                print('column_to_rel_column')
                cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s;", (user_link.link_id,))
                link = cursor.fetchone()
                title = link.title
                description = link.description
                keywords = link.keywords

                cursor = yield connection.execute("SELECT * FROM base_link WHERE is_active = %s AND site_id != %s ORDER BY created DESC LIMIT 100;", (True, user_link.site_id,))
                links = cursor.fetchall()
                result = related_to(link, links)
                print('res', result)

                cursor = yield connection.execute("SELECT * FROM base_link WHERE id IN (" + ','.join((str(n) for n in result)) + ");")
                items = cursor.fetchall()
                print('items', items)
                result_items = []

                cursor = yield connection.execute(
                            "INSERT INTO user_column (is_active, user_id, wall_id, site_id, name, type, color, icon, related_link_id) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}') RETURNING id, name, icon, type, color;".format(
                                False, user, column.wall_id, column.site_id, 'Related', 4, '#8e44ad', 'http://contter.dev/images/icon-re.png', user_link.id,))
                item = cursor.fetchone()
                #cursor.close()

                cur = yield connection.execute(
                            "INSERT INTO usercolumn_walls (user_wall_id, usercolumn_wall_id) VALUES ({0}, {1});".format(
                                item.wall_id, item.id, ))
                cur.close()

                c = yield connection.execute(
                            "INSERT INTO user_column_settings (is_active, user_id, column_id, word_filter, word_exclude, sort, size, type) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id, column_id, sort, size, type;".format(
                                False, user, item.id, '', '', 0, post['size'], 1,))
                st = c.fetchone()
                print(c, item)
                c.close()
                data = {"id": item.id, "name": item.name, "is_active": False,
                                "icon": str(item.icon), "sort": 0, "items": [],
                                "types": item.type, "color": item.color, 'status': 'proccess', 'success': u'create',
                                "st": {"size": st.size, "sort": st.sort, "type": st.type}}

                if column_to_rel_column:
                    print('yes column_to_rel_column')
                    cur = yield connection.execute(
                            "INSERT INTO column_to_column (column_base_id, column_to_id) VALUES ({0}, {1});".format(
                                column_to_rel_column, item.id,))

                    cur.close()
                else:
                    print('no column_to_rel_column')
                    curs = yield connection.execute(
                            "INSERT INTO user_column (is_active, user_id, wall_id, name, type, color, icon) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}') RETURNING id, name, icon, type, color;".format(
                                True, user, column.wall_id, 'Related group', 4, '#8e44ad', 'http://contter.dev/images/icon-re.png',))
                    main = curs.fetchone()
                    curs.close()

                    curso = yield connection.execute(
                            "UPDATE user_column SET column_to_rel_column = {0} WHERE id = {1};".format(main.id, column.id))
                    curso.close()


                    cur = yield connection.execute(
                            "INSERT INTO column_to_column (column_base_id, column_to_id) VALUES ({0}, {1});".format(
                                main.id, item.id,))
                    cur.close()




                for it in items:
                    print('it', it)
                    cs = yield connection.execute(
                            "INSERT INTO user_column_link (is_active, user_id, site_id, link_id, column_id,  site, related, related_children) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id, is_active, user_id, site_id, link_id, column_id,  site, related, related_children;".format(
                                True, user, user_link.site_id, it.id, item.id, False, True, user_link.id,))
                    ct = cs.fetchone()
                    i = {"id": ct.id, "title": it.title, "description": it.description, "tags": it.tags,
                         "small_img": it.small_img, "medium_img": it.medium_img, "large_img": it.large_img,
                         "url_path": it.url, "url": it.url}
                    result_items.append(i)
                    cs.close()

                data["items"] = result_items
                self.write({"success": u'success', "result": "top5", "data": data})
            else:
                item = user_column[0]
                cursor = yield connection.execute("SELECT * FROM user_column_settings WHERE user_id = %s AND column_id = %s;", (user, item.id, ))
                st = cursor.fetchone()
                #cursor.close()
                data = {"id": item.id, "name": item.name, "is_active": item.is_active,
                                "icon": str(item.icon), "sort": 0, "items": [],
                                "types": item.type, "color": item.color, 'status': 'proccess', 'success': u'create',
                                "st": {"size": st.size, "sort": st.sort, "type": st.type}}

                result_items = []

                #cursor = yield connection.execute("SELECT id FROM user_column_link WHERE column_id  ORDER BY link_id, created DESC LIMIT 10;")
                cursor = yield connection.execute("SELECT * FROM user_column_link WHERE is_active = %s AND column_id = %s ORDER BY created DESC LIMIT 10;", (True, item.id, ))
                links = cursor.fetchall()
                print('links', links)
                #cursor.close()

                for it in links:
                    cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s", (it.link_id,))
                    ite = cursor.fetchone()
                    #print('item', session.query(UserColumnLink).filter_by(id=it.id).update({"column_id": column.id}))
                    it = {"id": it.id, "title": ite.title, "description": ite.description, "tags": ite.tags,
                            "small_img": ite.small_img, "medium_img": ite.medium_img, "large_img": ite.large_img,
                            "url_path": ite.url, "url": ite.url}
                    result_items.append(it)
                    #cursor.close()

                data["items"] = result_items
                self.write({"success": u'success', "result": "top5", "data": data})

                #for li in links:
                    #print('li', li)
            """

        self.finish()


"""
#
# POST ReContent ADD
#
"""


class UserPostReContentAddHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserPostReContentAddHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'id' not in post:
            self.write({'failure': u'Error no link'})

        self.write({'failure': u'Error no link'})
        """
        connection = yield self.db.getconn()

        link = post['link']
        types = post['types']
        #o = urlparse(link)
        #print(o)
        if types == 'site':
            p_l, parse_link = validate_url(link)

            if not p_l:
                print('not validate')
                self.write({'failure': u'Error no link'})
            else:
                print('validate', parse_link)
                with self.db.manage(connection):
                    short_url = parse_link.netloc
                    cursor = yield connection.execute("SELECT short_url FROM base_site WHERE short_url = %s", (short_url,))
                    f = cursor.fetchall()
                    print(f)
                    if not f:
                        self.write({'failure': u'Site not base'})
                    elif len(f) > 1:
                        self.write({'success': u'many'})
                    else:
                        self.write({'success': u'one'})
                    print('ffff')
        """
        self.finish()


"""
#
# UserGetSetNotificationsHandler  user profile get noti
#
"""


class UserGetSetNotificationsHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserGetSetNotificationsHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        connection = yield self.db.getconn()

        if user:
            with self.db.manage(connection):
                cursor = yield connection.execute(
                        "SELECT * FROM user_to_notification WHERE user_id = '{0}' ORDER BY created DESC LIMIT 5;".format(
                                user))
                notis = cursor.fetchall()
                # cursor.close()

                notifs = []

                for noti in notis:
                    print(noti)
                    us, column = {}, {}
                    if noti.user_to_id:
                        cursor = yield connection.execute(
                                "SELECT * FROM users WHERE id = '{0}';".format(noti.user_to_id))
                        user_to = cursor.fetchall()
                        # cursor.close()
                        if user_to:
                            user_to = user_to[0]
                            us = {"id": user_to.id, "display_name": user_to.display_name, "twitter": user_to.twitter,
                                  "description": user_to.description, "first_name": user_to.first_name,
                                  "last_name": user_to.last_name, "username": user_to.username,
                                  "avatar": user_to.avatar,
                                  "twitter_img": user_to.twitter_img}
                    if noti.column_to_id:
                        cursor = yield connection.execute(
                                "SELECT * FROM user_column WHERE id = '{0}';".format(noti.column_to_id))
                        column_to = cursor.fetchall()
                        # cursor.close()
                        if column_to:
                            column_to = column_to[0]
                            column = {"id": column_to.id, "name": column_to.name, "color": column_to.name,
                                      "icon": column_to.icon}

                    notifs.append({"id": noti.id, "user": us, "types": noti.type, "action": noti.action,
                                   "column": column})

                print(notifs)
                self.write({'success': u'success', 'data': notifs})

        self.finish()


"""
#
# UserSetTypesHandler change type profile
#
"""


class UserSetTypesHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserSetTypesHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        connection = yield self.db.getconn()

        if user:
            with self.db.manage(connection):
                types = 1
                if post['types'] == u'ME':
                    types = 2

                cursor = yield connection.execute("UPDATE users SET type = {0} WHERE id = {1};".format(types, user))
                # cursor.close()

                self.write({'success': u'success'})

        self.finish()


"""
#
# UserSetColorHandler change color profile
#
"""


class UserSetColorHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserSetColorHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        connection = yield self.db.getconn()

        if user:
            with self.db.manage(connection):
                color = post['color']

                cursor = yield connection.execute("UPDATE users SET color = '{0}' WHERE id = {1};".format(color, user))
                # cursor.close()

                self.write({'success': u'success'})

        self.finish()


"""
#
# UserSetPictureHandler change picure profile
#
"""


class UserSetPictureHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserSetPictureHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = None  # json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print("UserSetPictureHandler", post, user)
        print(self.request.files)
        print(self.request.files['image'])

        connection = yield self.db.getconn()

        if user:
            with self.db.manage(connection):
                """
                file1 = self.request.files['image'][0]
                original_fname = file1['filename']
                extension = os.path.splitext(original_fname)[1]
                fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(9))
                final_filename = fname + str(user) + extension
                output_file = open("C:\OpenServer2\OpenServer\domains\contter.dev\media\\avatars\\" + final_filename, 'w')
                output_file.write(file1['body'])
                print(final_filename)
                """
                file1 = self.request.files['image'][0]
                original_fname = file1['filename']
                extension = os.path.splitext(original_fname)[1]
                fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(10))
                final_filename = fname + str(user) + extension
                f = open(r"C:\\OpenServer2\\OpenServer\\domains\\contter.dev\\media\\avatars\\" + final_filename, "w")
                im = Image.open(StringIO(file1["body"]))
                im.save(f, "JPEG")

                self.write({'success': u'success'})

        self.finish()


"""
#
# WALL
#
"""


class UserCreateWallHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserCreateWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    # @authenticateded
    def get(self):
        user = 1
        name = 'two'
        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute(
                    "INSERT INTO user_wall (is_active, user_id, name, slug,  description) VALUES ({0}, {1}, '{2}', '{3}', '{4}')".format(
                            True, user, name, name, ''))
            f = cursor.fetchone()
            # cursor.close()
            print(f)
            self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'name' not in post:
            self.write({'failure': u'Error no name'})

        connection = yield self.db.getconn()

        name = post['name']
        color = None
        sort = 1
        if 'color' in post:
            color = post['color']
        if 'sort' in post:
            sort = post['sort']
        # o = urlparse(link)
        # print(o)
        p_l = validate_wall_name(name)

        if not p_l:
            print('not validate')
            self.write({'failure': u'Error no link'})
            self.finish()
        else:
            with self.db.manage(connection):
                cursor = yield connection.execute(
                        "SELECT user_id FROM user_wall WHERE user_id = {0}; ".format(user))
                f = cursor.fetchall()
                # cursor.close()
                print(f)
                if len(f) > 30:
                    self.write({'failure': u'many too wals'})
                    self.finish()
                else:
                    n = datetime.datetime.now()
                    cursor = yield connection.execute(
                            u"INSERT INTO user_wall (is_active, user_id, name, slug,  description, color, sort, created) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}') RETURNING id, name, sort, color, icon".format(
                                    True, user, name.upper(), name.lower(), '', color, sort, n))
                    # cursor.close()
                    # cursor = yield connection.execute(
                    #        "SELECT * FROM user_wall WHERE name = '{0}' AND user_id = {1} ".format(name, user))
                    f = cursor.fetchone()
                    # cursor.close()
                    print('f', f)
                    data = {"name": f.name, "icon": f.icon, "sort": 1, 'columns': [], 'current': True,
                            "color": f.color}
                    self.write({'success': u'one', 'data': data})

        self.finish()


class UserSettingsWallHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserSettingsWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserSettingsWallHandler', post, user)

        types = post['types']
        wall_id = post['wall_id']
        value = post['value']
        data = {}

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                    "SELECT * FROM user_wall WHERE user_id = {0} AND id = {1}; ".format(user, wall_id))
            walls = cursor.fetchall()

            if walls:
                if types == 'title':
                    cursor = yield connection.execute(
                            "UPDATE user_wall SET name = '{0}', slug = '{1}'  WHERE id = {2};".format(
                                    value.upper(), value.lower(), wall_id))
                if types == 'color':
                    cursor = yield connection.execute(
                            "UPDATE user_wall SET color = '{0}'  WHERE id = {1};".format(
                                    value, wall_id))
                if types == 'home':
                    cursor = yield connection.execute(
                            "UPDATE user_wall SET home = {0}  WHERE id = {1};".format(
                                    True, wall_id))
                    cursor = yield connection.execute(
                            "UPDATE user_wall SET home = {0}  WHERE id != {1};".format(
                                    False, wall_id))
                if types == 'sort':
                    for s in value:
                        cursor = yield connection.execute(
                                "UPDATE user_wall SET sort = {0}  WHERE id = {1};".format(
                                        s['sort'], s['wall']))
                if types == 'delete':
                    cursor = yield connection.execute(
                            "UPDATE user_wall SET is_active = {0}  WHERE id = {1};".format(
                                    False, wall_id))

                self.write({'success': u'success', 'data': data, 'types': types})
            """"
            if len(f) > 30:
                self.write({'failure': u'many too wals'})
                self.finish()
            else:
                n = datetime.datetime.now()
                cursor = yield connection.execute(
                        "INSERT INTO user_wall (is_active, user_id, name, slug,  description, color, sort, created) VALUES ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')".format(
                                True, user, name, name, '', color, sort, n))
                # cursor.close()
                cursor = yield connection.execute(
                        "SELECT * FROM user_wall WHERE name = '{0}' AND user_id = {1} ".format(name, user))
                f = cursor.fetchone()
                # cursor.close()
                print('f', f)
                data = {"name": f.name, "icon": f.icon, "sort": 1, 'columns': [], 'current': True,
                        "color": f.color}
                self.write({'success': u'one', 'data': data})
            """

        self.finish()


class UserWelcomeWallHandler(BaseHandler):
    """
    first welcome create walls
    """

    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWelcomeWallHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserWelcomeWallHandler', post, user)
        start = time.time()

        self.write({'success': 'process'})
        """
        connection = yield self.db.getconn()
        with self.db.manage(connection):
            now = datetime.datetime.now()
            #WELCOME PAGE
            welcome_walls = post['walls']
            cursors = yield connection.execute("SELECT * FROM user_wall WHERE id IN (" + ','.join((str(n['id']) for n in welcome_walls)) + "); ")
            w_walls = cursors.fetchall()

            sql = u'INSERT INTO user_wall (is_active, name, icon, color,' \
                  u'slug, sort, home, created, description, user_id) VALUES {} RETURNING id, home;'.format(
                    ', '.join(["(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"] * len(w_walls)))
            params = []
            sort = 1
            for y in w_walls:
                home = True if sort == 1 else False
                params.extend([True, y.name, y.icon, y.color, y.slug, sort, home,
                               datetime.datetime.now(), y.description, user])
                sort += 1

            print('params', params)
            print('sql', sql)
            cursor = yield connection.execute(sql, params)
            #items = []

            bases = cursor.fetchall()
            r_bases = list(reversed(bases))
            print('r_bases', r_bases)
            yi = 0
            for base in r_bases:
                print('base', base.id)



                # IT IS WORKING
                welcome_columns = welcome_walls[yi]["columns"]
                cursor = yield connection.execute(
                    "SELECT * FROM user_column WHERE id IN (" + ','.join((str(n['id']) for n in welcome_columns)) + ");")
                base_columns = cursor.fetchall()
                print('base_columns', base_columns)

                # CREATE COLUMN
                sql = u"INSERT INTO user_column (is_active, user_id, wall_id, name, type," \
                      u"color, icon, created, site_id, description) VALUES {} " \
                      u"RETURNING id, name, icon, type, color;".format(
                        ', '.join(["(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"] * len(base_columns)))
                params = []
                for bs in base_columns:
                    params.extend([True, user, base.id, bs.name, 1, bs.color, bs.icon, now, bs.site_id,
                                   bs.description])
                print('p create column', params, sql)

                cursor = yield connection.execute(sql, params)
                user_columns = cursor.fetchall()

                # CREATE MODEL TO WALLS
                sql = "INSERT INTO usercolumn_walls (user_wall_id, usercolumn_wall_id) VALUES {} ".format(
                        ', '.join(["(%s, %s)"] * len(user_columns)))
                params = []
                for uc in user_columns:
                    params.extend([base.id, uc.id])
                print('p to walls', params, sql)

                cursor = yield connection.execute(sql, params)

                # CREATE COLUMN SETTINGS
                sql = "INSERT INTO user_column_settings (is_active, user_id, column_id, word_filter, word_exclude, sort, size, type) VALUES {}".format(
                        ', '.join(["(%s, %s, %s, %s, %s, %s, %s, %s)"] * len(user_columns)))
                params = []
                for uc in user_columns:
                    params.extend([True, user, uc.id, '', '', 0, 400, 1])
                print('p settings', params, sql)

                cursor = yield connection.execute(sql, params)

                # CREATE COLUMN SORT
                sql = "INSERT INTO user_column_wall_sort (sort, user_id, column_id, wall_id) VALUES {}".format(
                        ', '.join(["(%s, %s, %s, %s)"] * len(user_columns)))
                params = []
                sort = 1
                for uc in user_columns:
                    params.extend([sort, user, uc.id, base.id])
                    sort += 1
                print('p sort', params, sql)

                cursor = yield connection.execute(sql, params)


                # SELECT BASE LINK
                for column in base_columns:
                    cursor = yield connection.execute(
                        "SELECT * FROM base_link WHERE is_active = {0} AND site_id = {1}  "
                        "ORDER BY created DESC LIMIT 1; ".format(True, column.site_id)) #column.site_id

                    base_links = cursor.fetchall()
                    sql = 'INSERT INTO user_column_link (is_active, user_id, link_id, site_id, ' \
                          'column_id, site, recontent, related, created) VALUES ' \
                          '{};'.format(', '.join(["(%s, %s, '%s', '%s', '%s', '%s', '%s', '%s', %s)"] * len(base_links)))
                    params = []
                    for y in base_links:
                        params.extend([True, user, y.id, column.site_id, column.id, True, False, False, y.created])
                        #column.site_id #column
                    print('create item', sql, params)
                    cursor = yield connection.execute(sql, params)

                if base.home:
                    self.write({'success': 'process'})

                yi += 1

        print('end', time.time() - start)
        self.write({'success': 'finish'})
        """

        self.finish()


class UserWallHandler(BaseHandler):
    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        data, walls = {}, []

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute(
                    "SELECT * FROM user_wall WHERE user_id = %s AND is_active = %s ORDER BY sort ASC;",
                    (user, True,))
            wallss = cursor.fetchall()
            for f in wallss:
                columns = []

                c = yield connection.execute("SELECT * FROM usercolumn_walls WHERE user_wall_id = %s; ",
                                             (f.id,))
                walls_ids = [z.usercolumn_wall_id for z in c.fetchall()]
                walls_ids.append(0)
                cursor = yield connection.execute(
                        "SELECT * FROM user_column WHERE id IN (" + ','.join((str(n) for n in walls_ids)) + "); ")
                columnss = cursor.fetchall()

                for column in columnss:
                    columns.append({"id": column.id, "name": column.name, "icon": column.icon,
                                    "site_id": column.site_id, "types": column.type, "color": column.color})

                walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': False, "color": f.color,
                              "slug": f.slug, "description": f.description, "sort": f.sort, "columns": columns,
                              "home": f.home})

        data['walls'] = walls
        self.write(json_encode(data))
        self.set_header("Content-Type", "application/json")
        self.finish()


"""
#
# UserWallListHandler
#
"""


class UserWallListHandler(BaseHandler):
    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        user = self.current_user
        post = json.loads(self.request.body.decode('utf-8'))
        print('UserWallListHandler post, ', post)
        data, walls = {}, []
        if user:
            connection = yield self.db.getconn()
            with self.db.manage(connection):
                cursor = yield connection.execute(
                        "SELECT * FROM user_wall WHERE user_id = %s AND is_active = %s ORDER BY sort ASC;",
                        (user, True,))
                wallss = cursor.fetchall()
                for f in wallss:
                    walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': False, "color": f.color,
                                  "slug": f.slug, "description": f.description})
        data['walls'] = walls

        self.write(json_encode(data))
        self.set_header("Content-Type", "application/json")
        self.finish()


class UserWallFilterHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallFilterHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserWallFilterHandler', post, user)

        username, slugwall = None, None
        data, columns, walls = {}, [], []
        userType, userTypeColumn = 'Anonymus', 'Anonymus'

        if 'username' in post:
            username = post['username']

        if 'slugwall' in post:
            slugwall = post['slugwall']

        """

        connection = yield self.db.getconn()
        f_user, user_wall = None, None
        with self.db.manage(connection):
            if username and slugwall:
                cursor = yield connection.execute("SELECT * FROM users WHERE username = %s AND is_active = %s;",
                                                  (username, True,))
                ft = cursor.fetchall()
                f_user = ft[0] if ft else None
                if not f_user:
                    self.write({'failure': 'no user'})
                else:
                    cursor = yield connection.execute("SELECT * FROM user_wall WHERE user_id = %s AND is_active = %s AND slug = %s;",
                                                     (f_user.id, True, slugwall))
                    user_wall = cursor.fetchone()


            # If have wall and user
            if f_user and user_wall:
                # USER COLUMNS
                cursor = yield connection.execute("SELECT * FROM usercolumn_walls WHERE user_wall_id = %s; ",
                                                 (user_wall.id,))
                column_ids = [z.usercolumn_wall_id for z in cursor.fetchall()]
                column_ids.append(0)
                cursor = yield connection.execute(
                        "SELECT * FROM user_column WHERE id IN (" + ','.join((str(n) for n in column_ids)) + "); ")
                user_columns = cursor.fetchall()
                print('user_columns', user_columns)

                # DEFINE USER TYPE
                if user:
                    userType = 'Current' if f_user.id == user else 'Alien'

                # USER WALLS
                cursor = yield connection.execute("SELECT * FROM user_wall WHERE user_id = {0}"
                                                  " ORDER BY sort ASC;".format(f_user.id))
                for w in cursor.fetchall():
                    current = False
                    if w.id == user_wall.id:
                        current = True
                    walls.append({"id": w.id, "name": w.name, "icon": w.icon, "current": current, "color": w.color,
                                  "slug": w.slug, "userType": userType, "sort": w.sort, "home": w.home})

                print("walls", walls)

                # SORT USER COLUMNS
                sorter = []

                cursor = yield connection.execute(
                        "SELECT * FROM user_column_wall_sort WHERE wall_id = {0}"
                        " ORDER BY sort ASC;".format(user_wall.id))

                for sat in cursor.fetchall():
                    sorter.append({"wall_id": user_wall.id, "column_id": sat.column_id, "sort": sat.sort})

                print('sorter', sorter)

                for column in user_columns:
                    print("column", column)

                    # settings
                    cursor = yield connection.execute("SELECT * FROM user_column_settings WHERE"
                                                      " user_id = {0} AND column_id = {1};".format(user, column.id))
                    set = cursor.fetchone()

                    set_word_exclude, set_word_filter = [], []
                    try:
                        set_word_exclude = set.word_exclude.split(', ')
                    except:
                        pass
                    try:
                        set_word_filter = set.word_filter.split(', ')
                    except:
                        pass

                    st = {"word_filter": set_word_filter, "word_exclude": set_word_exclude, "sort": set.sort,
                          "size": set.size, "type": set.type, "realtime": set.realtime}

                    # column walls
                    column_walls = []
                    #cst = session.execute(text("SELECT * FROM usercolumn_walls WHERE usercolumn_wall_id = " + str(column.id) + ";"))
                    #cws, column_walls = [f.user_wall_id for f in cst], []
                    #for w in walls:
                        #if w['id'] in cws:
                            #column_walls.append(w)

                    items = []

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_link WHERE column_id = {0} AND is_active ={1} ORDER BY created DESC LIMIT 10;".format(column.id, True))

                    for it in cursor.fetchall():
                        cursor = yield connection.execute("SELECT * FROM base_link WHERE id = {0};".format(it.link_id,))
                        item = cursor.fetchone()

                        it = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                              "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                              "url_path": item.url, "url": item.url, "thumbnail": item.thumbnail,
                              "json_img": item.json_img,
                              "if_recontent": it.if_recontent, "if_favourite": it.if_favourite,
                              "if_recontent_id": it.if_recontent_id, "if_favourite_id": it.if_favourite_id}
                        items.append(it)

                    print('items', items)

                    try:
                        sort = filter(lambda n: n.get('column_id') == column.id, sorter)[0]['sort']
                    except:
                        sort = 1

                    userTypeColumn = 'Current'

                    col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": sort, "items": items,
                           "site_id": column.site_id,
                           "types": column.type, "st": st, "color": column.color, "recontent_items": [],
                           "if_recontent": False, "get_recontent": False,
                           "column_to_rel_column": column.column_to_rel_column, "related_to_items": [],
                           "column_walls": column_walls, "wall_id": user_wall.id, "launch": "standart",
                           "favourites_items": [], "if_favourites": False, "get_favourites": False,
                           "userTypeColumn": userTypeColumn}

                    columns.append(col)

                data = {"id": user_wall.id, "name": user_wall.name, "color": user_wall.color,
                        "icon": user_wall.icon, "sort": 1, "columns": columns, "walls": walls,
                        "slug": user_wall.slug, "userType": userType}
                print('data', data)
                self.write(data)

            #
            #
            #user = session.query(User).filter(User.username == username).first()
            #user_wall = session.query(UserWall).filter(UserWall.is_active == True,  UserWall.user_id == user.id, UserWall.slug == slugwall).order_by(
            #desc(UserWall.created)).first()
        """
        data = to_json(user=user, username=username, slugwall=slugwall)
        self.write(data)

        self.finish()


class UserWallSitesHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallSitesHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserWallSitesHandler', post, user)
        short_url = post['short_url']

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute("SELECT * FROM base_site WHERE short_url = %s AND is_active = %s;",
                                              (short_url, True,))
            sites = cursor.fetchall()
            # cursor.close()

            if not sites:
                self.write({'failure': 'no site'})
                self.finish()
            else:
                columns = []
                for site in sites:
                    items = []

                    cursor = yield connection.execute(
                            "SELECT * FROM base_link WHERE site_id = %s AND is_active = %s ORDER BY created ASC LIMIT 15;",
                            (site.id, True,))
                    itemss = cursor.fetchall()
                    # cursor.close()
                    for it in itemss:
                        its = {"id": it.id, "title": it.title, "description": it.description, "tags": it.tags,
                               "small_img": it.small_img, "medium_img": it.medium_img, "large_img": it.large_img,
                               "url_path": it.url, "url": it.url}
                        items.append(its)

                    col = {"id": site.id, "name": site.name, "icon": site.favicon, "sort": 0, "items": items,
                           "site_id": site.id, "types": 1, "st": {"size": 400}, "color": site.color,
                           "description": site.description,
                           "url": site.url, "launch": "sites"}
                    columns.append(col)

                data = {"id": 0, "name": short_url, "color": '#ffffff', "icon": '', "sort": 1, 'columns': columns,
                        'walls': [], "slug": '', "userType": ''}

                self.write(data)
        self.finish()


"""
#
# Trends page
#
"""


class UserWallTrendsHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallTrendsHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        post['short_url'] = 'espn.go.com'
        print('UserWallTrendsHandler', post, user)
        short_url = post['short_url']

        connection = yield self.db.getconn()
        # cursor = yield connection.execute("SELECT * FROM base_site WHERE short_url = %s AND is_active = %s;", (short_url, True,))
        # sites = cursor.fetchall()
        # #cursor.close()
        with self.db.manage(connection):

            cursor = yield connection.execute("SELECT * FROM user_column WHERE type = %s ORDER BY created DESC LIMIT 15;",
                                              (6,))
            columnss = cursor.fetchall()
            columns = []
            # cursor.close()

            if not columnss:
                pass
                # self.write({'failure': 'no columns'})
            else:

                for column in columnss:
                    cursor = yield connection.execute("SELECT * FROM user_column_settings WHERE column_id = %s;", (column.id, ))
                    st = cursor.fetchall()

                    try:
                        word_filter = st[0].word_filter.split(', ')
                    except:
                        word_filter = []

                    try:
                        word_exclude = st[0].word_exclude.split(', ')
                    except:
                        word_exclude = []

                    items = []

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_link WHERE column_id = %s AND is_active = %s ORDER BY created ASC LIMIT 15;",
                            (column.id, True,))
                    itemss = cursor.fetchall()
                    # cursor.close()
                    for it in itemss:
                        cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s;", (it.link_id,))
                        item = cursor.fetchone()
                        # cursor.close()
                        its = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                               "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                               "url_path": item.url, "url": item.url, "json_img": item.json_img}
                        items.append(its)

                    col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": 0, "items": items,
                           "site_id": column.site_id, "types": 6, "st": {"size": 400,
                                                                         "word_filter": word_filter,
                                                                         "word_exclude": word_exclude,
                                                                         }, "color": column.color,
                           "description": column.description,
                           "url": '', "launch": "sites", "is_active": column.is_active}
                    columns.append(col)

            walls = []
            if user:
                cursor = yield connection.execute(
                    "SELECT * FROM user_wall WHERE user_id = %s AND is_active = %s ORDER BY sort ASC LIMIT 20;",
                    (user, True,))
                wallss = cursor.fetchall()
                for f in wallss:
                    walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': False, "color": f.color,
                                  "slug": f.slug, "sort": f.sort, "home": f.home})

            data = {"id": 0, "name": short_url, "color": '#ffffff', "icon": '', "sort": 1, 'columns': columns,
                    'walls': walls, "slug": '', "userType": 'userType'}

            self.write(data)

        self.finish()


"""
#
# Top page
#
"""


class UserWallTopHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallTopHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        cat = post['cat']
        columns = []
        print('UserWallTopHandler', post, user)

        connection = yield self.db.getconn()
        with self.db.manage(connection):

            columnss = None

            if cat == u'RECOMMENDED':
                cursor = yield connection.execute("SELECT * FROM user_column WHERE type = %s AND is_active = %s;",
                                                  (7, True,))
                columnss = cursor.fetchall()
                # cursor.close()

            else:
                columnss = []
                cat_id = post['id']
                # cursor = yield connection.execute("SELECT * FROM user_wall WHERE id = %s AND is_active = %s AND is_top = %s;",
                #                                      (cat_id, True, True))

                cursor = yield connection.execute(
                        "SELECT * FROM user_column_wall_sort WHERE wall_id = {0} ORDER  BY sort ASC;".format(
                                cat_id, ))
                column_walls = cursor.fetchall()
                # cursor.close()
                for cw in column_walls:
                    cursor = yield connection.execute("SELECT * FROM user_column WHERE id = %s;",
                                                      (cw.column_id, ))
                    cs = cursor.fetchall()
                    # cursor.close()
                    if cs:
                        columnss.append(cs[0])

            if columnss:

                for column in columnss:
                    cursor = yield connection.execute("SELECT * FROM user_column_settings WHERE column_id = %s;", (column.id, ))
                    st = cursor.fetchall()

                    try:
                        word_filter = st[0].word_filter.split(', ')
                    except:
                        word_filter = []

                    try:
                        word_exclude = st[0].word_exclude.split(', ')
                    except:
                        word_exclude = []

                    items = []

                    cursor = yield connection.execute(
                            "SELECT * FROM user_column_link WHERE column_id = %s AND is_active = %s ORDER BY created ASC LIMIT 15;",
                            (column.id, True,))
                    itemss = cursor.fetchall()
                    # cursor.close()
                    for it in itemss:
                        cursor = yield connection.execute("SELECT * FROM base_link WHERE id = %s;", (it.link_id,))
                        item = cursor.fetchone()
                        # cursor.close()
                        its = {"id": it.id, "title": item.title, "description": item.description, "tags": item.tags,
                               "small_img": item.small_img, "medium_img": item.medium_img, "large_img": item.large_img,
                               "url_path": item.url, "url": item.url, "json_img": item.json_img}
                        items.append(its)

                    col = {"id": column.id, "name": column.name, "icon": column.icon, "sort": 0, "items": items,
                           "site_id": column.site_id, "types": 6, "st": {"size": 400,
                                                                         "word_filter": word_filter,
                                                                         "word_exclude": word_exclude,
                                                                         }, "color": column.color,
                           "description": column.description,
                           "url": '', "launch": "sites", "is_active": column.is_active}
                    columns.append(col)

            walls = []
            if user:
                cursor = yield connection.execute(
                    "SELECT * FROM user_wall WHERE user_id = %s AND is_active = %s ORDER BY sort ASC LIMIT 20;",
                    (user, True,))
                wallss = cursor.fetchall()
                for f in wallss:
                    walls.append({'id': f.id, 'name': f.name, 'icon': f.icon, 'current': False, "color": f.color,
                                  "slug": f.slug, "sort": f.sort, "home": f.home})

            data = {"id": 0, "name": '', "color": '#ffffff', "icon": '', "sort": 1, 'columns': columns,
                    'walls': walls, "slug": '', "userType": 'userType'}

            self.write(data)

        self.finish()


"""
#
#
#
"""


class UserWallWallsHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallWallsHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        cat = post['cat']
        wallss = []
        print('UserWallWallsHandler', post, user)

        connection = yield self.db.getconn()
        with self.db.manage(connection):

            if cat == u'RECOMMENDED':
                pass
                #cursor = yield connection.execute("SELECT * FROM user_column WHERE type = %s AND is_active = %s;",
                #                                  (7, True,))
               #walss = cursor.fetchall()
                # cursor.close()

            else:

                cat_id = post['id']
                # cursor = yield connection.execute("SELECT * FROM user_wall WHERE id = %s AND is_active = %s AND is_top = %s;",
                #                                      (cat_id, True, True))

                cursor = yield connection.execute(
                        "SELECT * FROM user_wall WHERE category_id = {0} ORDER BY sort ASC LIMIT 15;".format(cat_id))

                for f in cursor.fetchall():
                    wall_columns = []

                    cursor = yield connection.execute("SELECT * FROM userwall_sites WHERE userwall_wall_id = %s; ", (f.id,))
                    column_ids = [z.userwall_site_id for z in cursor.fetchall()]
                    column_ids.append(0)
                    print("column_ids", column_ids)
                    cursors = yield connection.execute(
                        "SELECT * FROM base_site WHERE id IN (" + ','.join((str(n) for n in column_ids)) + "); ")

                    for column in cursors.fetchall():
                        print("column", column)
                        col = {"id": column.id, "name": column.name, "icon": column.favicon, "sort": 0, "items": [],
                           "types": 8, "st": {"size": 400}, "color": column.color,
                           "description": column.description,
                           "url": '', "launch": "walls"}
                        wall_columns.append(col)

                    wallss.append({'id': f.id, "name": f.name, "icon": f.icon, "color": f.color,
                                   "slug": f.slug, "category_id": f.category_id, "description": f.description,
                                   "thumbnail": f.thumbnail, "medium_img": f.medium_img, "columns": wall_columns})



            walls = []
            if user:

                data = {"id": 0, "name": '', "color": '#ffffff', "icon": '', "sort": 1, 'wallss': wallss,
                        'walls': walls, "slug": '', "userType": 'userType'}

                self.write(data)

        self.finish()


"""
#
# Category page
#
"""


class UserWallCategoryHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWallCategoryHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserWallCategoryHandler', post, user)

        data = {}

        connection = yield self.db.getconn()
        with self.db.manage(connection):

            if post['types'] == 'is_collections':
                cursor = yield connection.execute(
                        "SELECT * FROM category_model WHERE is_collections = %s ORDER BY sort ASC;",
                        (True,))
                categories = cursor.fetchall()
                # cursor.close()
                print('categories', categories)
                if not categories:
                    self.write({'failure': 'no columns'})
                else:
                    categoriess = []
                    for cat in categories:
                        categoriess.append({"id": cat.id, "name": cat.name, "description": cat.description,
                                            "is_collections": cat.is_collections, "is_active": cat.is_active,
                                            "sort": cat.sort})
                    data['categories'] = categoriess
                    self.write(data)
            else:

                cursor = yield connection.execute(
                        "SELECT * FROM user_wall WHERE is_top = %s ORDER BY sort ASC;", (True,))
                categories = cursor.fetchall()
                # cursor.close()
                print('categories', categories)
                if not categories:
                    self.write({'failure': 'no columns'})
                else:
                    categoriess = []
                    for cat in categories:
                        categoriess.append({"id": cat.id, "name": cat.name, "description": cat.description,
                                            "is_top": cat.is_top, "is_active": cat.is_active, "sort": cat.sort})
                    data['categories'] = categoriess
                    self.write(data)

        self.finish()


"""
#
# WELCOME PAGE
#
"""
from extra import collect

class UserWelcomeHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserWelcomeHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserWelcomeHandler', post, user)

        data = {"collect": collect}

        self.write(data)
        self.finish()


class UserSlugWallHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        print('fdfdf', self.get_argument("id"))
        id = self.get_argument("id", default=None)
        data = to_json_id(id)
        self.write(json_encode(data))
        self.set_header("Content-Type", "application/json")

        self.finish()


"""
#
# User GET
#
"""


class UserGetHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserGetHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print(post, user)

        if 'username' not in post:
            self.write({'failure': u'no username'})
        else:
            connection = yield self.db.getconn()

            with self.db.manage(connection):
                cursor = yield connection.execute("SELECT * FROM users WHERE username = %s AND is_active = %s;",
                                                  (post["username"], True,))
                f = cursor.fetchall()
                # cursor.close()
                if not f:
                    self.write({'failure': u'no user'})
                else:
                    us = f[0]

                    cursor = yield connection.execute("SELECT COUNT(*) FROM user_followers WHERE user_base_id = %s;",
                                                      (us.id,))
                    followers_count = cursor.fetchone().count
                    # cursor.close()

                    cursor = yield connection.execute("SELECT COUNT(*) FROM user_following WHERE user_base_id = %s;",
                                                      (us.id,))
                    following_count = cursor.fetchone().count
                    # cursor.close()

                    cursor = yield connection.execute(
                            "SELECT COUNT(*) FROM user_wall WHERE user_id = %s AND is_active = %s;", (us.id, True,))
                    walls_count = cursor.fetchone().count
                    # cursor.close()
                    print('walls_count', walls_count)

                    cursor = yield connection.execute(
                            "SELECT COUNT(*) FROM user_column_link WHERE is_active = %s AND recontent = %s AND user_id = %s;",
                            (True, True, us.id,))
                    recontent_count = cursor.fetchone().count
                    # cursor.close()
                    print('recontent_count', recontent_count)

                    current = False
                    follow = False
                    if us.id == user:
                        current = True
                        cursor = yield connection.execute(
                                "SELECT * FROM user_following WHERE user_base_id = %s AND user_to_id = %s",
                                (user, us.id,))
                        following = cursor.fetchall()
                        # cursor.close()
                        if following:
                            follow = True

                    userType = 'Alien'
                    if us.id == user:
                        userType = 'Current'

                    print('us_user', us)
                    try:
                        name = us.first_name + ' ' + us.last_name
                    except:
                        name = us.first_name
                    data = {"id": us.id, "email": us.email, "display_name": us.display_name, "twitter": us.twitter,
                            "description": us.description, "first_name": us.first_name, "last_name": us.last_name,
                            "name": name, "username": us.username, "avatar": us.avatar, "twitter_img": us.twitter_img,
                            "current": current, "follow": follow, "followers_count": followers_count,
                            "walls_count": walls_count, "recontent_count": recontent_count,
                            "following_count": following_count, "UserType": userType, "color": us.color,
                            "types": us.type, "new": us.new}

                    self.write({'success': u'success', 'data': data})

                    """
                    dict(id=self.id, email=self.email, displayName=self.display_name,
                    facebook=self.facebook, google=self.google,
                    linkedin=self.linkedin, twitter=self.twitter, new=self.new,
                    description=self.description, first_name=self.first_name, last_name=self.last_name,
                    name=self.name())
                    """

                    # self.write({'success': u'many'})
        """
        connection = yield self.db.getconn()

        link = post['link']
        types = post['types']
        #o = urlparse(link)
        #print(o)
        if types == 'site':
            p_l, parse_link = validate_url(link)

            if not p_l:
                print('not validate')
                self.write({'failure': u'Error no link'})
            else:
                print('validate', parse_link)
                with self.db.manage(connection):
                    short_url = parse_link.netloc
                    cursor = yield connection.execute("SELECT short_url FROM base_site WHERE short_url = %s", (short_url,))
                    f = cursor.fetchall()
                    print(f)
                    if not f:
                        self.write({'failure': u'Site not base'})
                    elif len(f) > 1:
                        self.write({'success': u'many'})
                    else:
                        self.write({'success': u'one'})
                    print('ffff')
        """
        self.finish()


class UserFollowHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserFollowHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        action = post['action']
        id = post['id']
        print(post, user)
        if action == u'follow':
            print('follow')
        elif action == u'unfollow':
            print('unfollow')

        self.finish()


"""
#
# User GET
#
"""


class UserGetMeHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserGetMeHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @anonymous_authenticateded
    def get(self):
        user = self.current_user
        print('UserGetMeHandler get', user)
        if user:
            connection = yield self.db.getconn()
            with self.db.manage(connection):
                cursor = yield connection.execute("SELECT * FROM users WHERE id = %s;", (user,))
                f = cursor.fetchall()
                # cursor.close()
                us = f[0]
                data = {"id": us.id, "email": us.email, "display_name": us.display_name, "twitter": us.twitter,
                        "description": us.description, "first_name": us.first_name, "last_name": us.last_name,
                        "username": us.username, "avatar": us.avatar, "twitter_img": us.twitter_img,
                        "displayName": us.display_name, "color": us.color, "types": us.type, "new": us.new}
                self.write(data)
        else:
            self.write({"nouser": "nouser"})

        self.finish()

    @gen.coroutine
    @anonymous_authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserGetMeHandler', post, user)
        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute("SELECT * FROM users WHERE id = %s;", (user,))
            f = cursor.fetchall()
            # cursor.close()
            us = f[0]
            data = {"id": us.id, "email": us.email, "display_name": us.display_name, "twitter": us.twitter,
                    "description": us.description, "first_name": us.first_name, "last_name": us.last_name,
                    "username": us.username, "avatar": us.avatar, "twitter_img": us.twitter_img,
                    "displayName": us.display_name, "color": us.color, "types": us.type, "new": us.new}
            self.write(data)

        self.finish()


# TWITTER AUTH
twitterAPI = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)


# recommendations from twitter
def recommendations_from_twitter(api):
    print('recommendations_from_twitter', api)
    if api:
        public_tweets = api.home_timeline()
        for tweet in public_tweets:
            print(tweet)


class UserAuthTwitterHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(UserAuthTwitterHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    def get(self):
        user = self.current_user
        # get = json.loads(self.request.body.decode('utf-8'))
        print('UserAuthTwitterHandler get', user)

        self.finish()

    @gen.coroutine
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('UserAuthTwitterHandler post', post, user)

        request_token_url = 'https://api.twitter.com/oauth/request_token'
        access_token_url = 'https://api.twitter.com/oauth/access_token'

        if 'oauth_token' in post and 'oauth_verifier' in post:
            auth = OAuth1(TWITTER_CONSUMER_KEY,
                          client_secret=TWITTER_CONSUMER_SECRET,
                          resource_owner_key=post['oauth_token'],
                          verifier=post['oauth_verifier'])
            r = requests.post(access_token_url, auth=auth)
            profile = dict(parse_qsl(r.text))

            print('profile', profile, profile['screen_name'])

            connection = yield self.db.getconn()
            with self.db.manage(connection):

                name, us_info, description, extra, api = '', {}, '', {}, None

                try:
                    twitterAPI.set_access_token(profile['oauth_token'], profile['oauth_token_secret'])
                    api = tweepy.API(twitterAPI, parser=JSONParser())
                    us_info = api.me()

                    if 'name' in us_info:
                        name = us_info['name']
                    if 'description' in us_info:
                        description = description

                    z1 = dict(profile.items() + us_info.items())
                    ext = json.dumps(z1)
                    extra = ext.replace("'", "''")
                    print(extra)
                except:
                    pass

                cursor = yield connection.execute("SELECT * FROM  users WHERE twitter = %s;", (profile['user_id'],))
                f = cursor.fetchall()
                if f:

                    cursor = yield connection.execute(
                            "UPDATE users SET first_name = '{0}' , extra = '{1}' , new = {2} WHERE id = {3};".format(
                                    name, extra, True, f[0].id))

                    token = create_token(f[0])
                    self.write({"token": token, "new": True})
                    print('no')

                    # recommendations_from_twitter(api)

                else:
                    now = datetime.datetime.now()
                    cursor = yield connection.execute(
                            "INSERT INTO users (twitter, display_name, username, extra, type, color, first_name, description, is_active, created, updated, new) VALUES ({0}, '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', '{8}', '{9}', '{10}' '{11}') RETURNING id, twitter, display_name, username, extra;".format(
                                    profile['user_id'], profile['screen_name'], profile['screen_name'], extra, 1,
                                    '50cc6d', name, description, True, now, now, True))

                    u = cursor.fetchone()
                    token = create_token(u)
                    self.write({"token": token, "new": True})
                    print('yes')

        else:
            oauth = OAuth1(TWITTER_CONSUMER_KEY,
                           client_secret=TWITTER_CONSUMER_SECRET,
                           callback_uri=TWITTER_CALLBACK_URL)
            r = requests.post(request_token_url, auth=oauth)
            oauth_token = dict(parse_qsl(r.text))
            self.write(oauth_token)

        self.finish()


def fer():
    test = ''
    with open('C:\Users\user\PycharmProjects\contest2\wall_one.json', 'r') as f:
        test = f.read()
        f.close()
    return test


cl = []

conn = psycopg2.connect(dsn)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)


def receive(fd, events):
    print('yes', fd, events, conn.notifies)
    '''Receive a notify message from the channel we are listening.'''
    state = conn.poll()
    if state == psycopg2.extensions.POLL_OK:
        if conn.notifies:
            notify = conn.notifies.pop()
            noti = json.loads(notify.payload)
            for ws in cl:
                print('ws.current_user', ws.current_user)
                if str(noti['type']) == "USER":
                    if int(noti["user"]) == ws.current_user:
                        print('esdsdsd', ws, )
                        ws.send_messages(notify.payload)
                        # ws.send_messages2(noti)
                        # ws.send_handler_user(noti)
                elif str(noti['type']) == "ALL":
                    pass
                    # ws.handler_websocket(noti, "ALL")

            print(notify.payload)
            print(json.loads(notify.payload))
            # p = {"cmd": "INSERT", "type": "USER", "user": 1, "name": "ws_new_links", "data": {"id": 2, "columnn_id": 3}}


def handler_websocket(self, data, type, ws):
    print('handlerrr')
    return_data = data["data"]

    # notifoication about new post in user column
    if data['name'] == 'ws_new_links':
        id = return_data["id"]

    # return result
    ws.send_messages(json.dumps(data))


class WebSocketHandlerTest(BaseHandlerWebSocket, WebSocketHandler):
    connections = set()

    def open(self):
        if self not in cl:
            cl.append(self)
        WebSocketHandlerTest.connections.add(self)

    def on_close(self):
        if self in cl:
            cl.remove(self)
        WebSocketHandlerTest.connections.remove(self)

    def on_message(self, msg):
        # print(msg)
        # print('open cl', cl)
        self.send_messages(msg)

    def send_messages(self, msg):
        for conn in self.connections:
            conn.write_message({'user': self.current_user, 'msg': msg})

    @coroutine
    def send_handler_user(self, data):
        print('handlerrr')
        user = self.current_user
        return_data = {}

        connection = yield self.db.getconn()

        print('ddd', data)
        # notifoication about new post in user column
        if str(data['name']) == 'ws_new_links':
            id = data["data"]["id"]
            with self.db.manage(connection):
                link = yield connection.execute("SELECT * FROM user_column_link WHERE user_id = %s AND id = %s;",
                                                (user, id,))
                li = link.fetchone()

                wall = yield connection.execute("SELECT * FROM user_column WHERE id = %s;", (li.column_id,))
                wl = wall.fetchone()

                print('link', link)
                item = yield connection.execute("SELECT * FROM base_link WHERE id = %s;", (li.link_id,))
                it = item.fetchone()
                print('item', it)
                return_data['id'] = id
                return_data['column_id'] = li.column_id
                return_data['wall_id'] = wl.wall_id
                return_data['post'] = {"description": it.description, "id": it.id, "large_img": it.large_img,
                                       "medium_img": it.medium_img, "small_img": it.small_img, "tags": it.tags,
                                       "title": it.title, "url": it.url, "url_path": it.full_url}

                data['data'] = return_data
                print('yesss', id, return_data)
                link.close()
                item.close()
                wall.close()
                self.send_messages(json.dumps(data))


                # return result
                # self.send_messages(json.dumps(data))

"""
#
# ADMIN
#
"""

class AdminNameColumnTrendHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminNameColumnTrendHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminNameColumnTrendHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                                    u"UPDATE user_column SET name = '{0}' WHERE id = {1}".format(
                                            post["name"], post["column_id"]))


class AdminNameColumnTopHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminNameColumnTopHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminNameColumnTopHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                                    u"UPDATE user_column SET name = '{0}' WHERE id = {1}".format(
                                            post["name"], post["column_id"]))

class AdminToggleColumnTrendHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminToggleColumnTrendHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminToggleColumnTrendHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                                    "UPDATE user_column SET is_active = {0} WHERE id = {1}".format(
                                            post["action"], post["column_id"]))

class AdminToggleColumnTopHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminToggleColumnTopHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminToggleColumnTopHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                                    "UPDATE user_column SET is_active = {0} WHERE id = {1}".format(
                                            post["action"], post["column_id"]))


class AdminToggleWallTopHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminToggleWallTopHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminToggleWallTopHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                                    "UPDATE user_wall SET is_active = {0} WHERE id = {1}".format(
                                            post["action"], post["wall_id"]))

class AdminToggleCategoryWallsHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminToggleCategoryWallsHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminToggleCategoryWallsHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                                    "UPDATE category_model SET is_active = {0} WHERE id = {1}".format(
                                            post["action"], post["wall_id"]))


class AdminCreateColumnTrendHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminCreateColumnTrendHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminCreateColumnTrendHandler', post, user)





        connection = yield self.db.getconn()
        start = time.time()
        now = datetime.datetime.now()

        with self.db.manage(connection):

            cursor = yield connection.execute(
                            u"INSERT INTO user_column (is_active,name, type, color, icon, created) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}') RETURNING id;".format(
                                    False, post["name"], 6, '', '', now, ))

            item = cursor.fetchone()

            c = yield connection.execute(
                                u"INSERT INTO user_column_settings (is_active,column_id, word_filter, word_exclude, sort, size, type) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}') RETURNING id, column_id, word_filter, word_exclude, sort, size, type;".format(
                                        True, item.id, ', '.join(post['matchings']),
                                        ', '.join(post['excludings']), 0,
                                        400, 1))
            st = c.fetchone()

class AdminCreateWallTopHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminCreateWallTopHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminCreateWallTopHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            now = datetime.datetime.now()
            cursors = yield connection.execute("SELECT * FROM user_wall WHERE is_top = %s", (True,))
            items = cursors.fetchall()
            sort = len(items) + 1

            cursor = yield connection.execute(
                            u"INSERT INTO user_wall (is_active, name, is_top, created, sort) VALUES ({0}, '{1}', '{2}', '{3}', {4}) RETURNING id, name, description, is_top, is_active, sort;".format(
                                    False, post["name"], True, now, sort))

            cat = cursor.fetchone()

            data = {"id": cat.id, "name": cat.name, "description": cat.description,
                                            "is_top": cat.is_top, "is_active": cat.is_active, "sort": cat.sort}
            self.write(data)


class AdminCreateWallWallsHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminCreateWallWallsHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        #post = json.loads(self.request.body.decode('utf-8'))
        args = self.request.arguments
        user = self.current_user
        print('AdminCreateWallWallsHandler', args, user)
        print(self.request.files)
        color = args['color'][0]
        cat_id = args['cat_id'][0]
        name = args['name'][0]
        desc = args['desc'][0]
        a_columns = args['columns'][0].split(',')
        print(color, a_columns)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            now = datetime.datetime.now()

            file1 = self.request.files['image'][0]
            original_fname = file1['filename']
            extension = os.path.splitext(original_fname)[1]
            fname = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(10))
            final_filename = fname + extension
            f = open(r"C:\\OpenServer2\\OpenServer\\domains\\contter.dev\\media\\walls\\" + final_filename, "w")
            im = Image.open(StringIO(file1["body"]))
            im.save(f, "JPEG")

            cursor = yield connection.execute(
                            u"INSERT INTO user_wall (is_active, name, is_collections, created, sort, category_id, description, color, medium_img, thumbnail) VALUES ({0}, '{1}', '{2}', '{3}', {4}, {5}, '{6}', '{7}', '{8}', '{9}') RETURNING id;".format(
                                    True, name, True, now, 0, cat_id, desc, color, final_filename, final_filename))

            wall = cursor.fetchone()

            for clmn in a_columns:
                cursor = yield connection.execute(
                            "INSERT INTO userwall_sites (userwall_wall_id, userwall_site_id) VALUES ({0}, {1});".format(
                                wall.id, clmn, ))

            """
            cursors = yield connection.execute("SELECT * FROM category_model WHERE is_collections = %s", (True,))
            items = cursors.fetchall()
            sort = len(items) + 1

            cursor = yield connection.execute(
                            u"INSERT INTO category_model (is_active, name, is_collections, created, sort) VALUES ({0}, '{1}', '{2}', '{3}', {4}) RETURNING id, name, description, is_collections, is_active, sort;".format(
                                    False, post["name"], True, now, sort))

            cat = cursor.fetchone()

            data = {"id": cat.id, "name": cat.name, "description": cat.description,
                    "is_collections": cat.is_collections, "is_active": cat.is_active, "sort": cat.sort}
            self.write(data)
            """

class AdminCreateCategoryWallsHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminCreateCategoryWallsHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminCreateCategoryWallsHandler', post, user)

        connection = yield self.db.getconn()

        with self.db.manage(connection):
            now = datetime.datetime.now()
            cursors = yield connection.execute("SELECT * FROM category_model WHERE is_collections = %s", (True,))
            items = cursors.fetchall()
            sort = len(items) + 1

            cursor = yield connection.execute(
                            u"INSERT INTO category_model (is_active, name, is_collections, created, sort) VALUES ({0}, '{1}', '{2}', '{3}', {4}) RETURNING id, name, description, is_collections, is_active, sort;".format(
                                    False, post["name"], True, now, sort))

            cat = cursor.fetchone()

            data = {"id": cat.id, "name": cat.name, "description": cat.description,
                    "is_collections": cat.is_collections, "is_active": cat.is_active, "sort": cat.sort}
            self.write(data)


class AdminCreateColumnTopHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminCreateColumnTopHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def get(self):
        user = self.current_user
        self.write('test')
        self.finish()

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminCreateColumnTopHandler', post, user)

        connection = yield self.db.getconn()
        start = time.time()
        now = datetime.datetime.now()

        with self.db.manage(connection):
            cursor = yield connection.execute(
                            u"INSERT INTO user_column (is_active,name, type, color, icon, created) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}') RETURNING id;".format(
                                    False, post["name"], 5, '', '', now, ))

            column = cursor.fetchone()

            cursor = yield connection.execute(
                            u"INSERT INTO user_column_wall_sort (is_active, wall_id, column_id, sort, created) VALUES ({0}, '{1}', '{2}', '{3}', '{4}') RETURNING id;".format(
                                    True, post["wall_id"], column.id, 0, now, ))

            cursor = yield connection.execute(
                                u"INSERT INTO user_column_settings (is_active,column_id, word_filter, word_exclude, sort, size, type) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}') RETURNING id, column_id, word_filter, word_exclude, sort, size, type;".format(
                                        True, column.id, ', '.join(post['matchings']),
                                        ', '.join(post['excludings']), 0,
                                        400, 1))

            cursor = yield connection.execute(
                        "SELECT * FROM user_column_wall_sort WHERE wall_id = {0} ORDER BY sort ASC;".format(post["wall_id"]))
            sorter = cursor.fetchall()

            i = 1
            for st in sorter:
                cursor = yield connection.execute(
                            "UPDATE user_column_wall_sort SET sort = {0} WHERE id = {1}".format(
                                    i, st.id))
                i += 1



class AdminTestHandler(BaseHandler):
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminTestHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        print('AdminTestHandler', post, user)
        self.write({"user": "yes"})
        self.finish()


class AdminGetUsersHandler(BaseHandler):
    """
    Get all users
    """
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminGetUsersHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        data, users = {}, []
        print('AdminTestHandler', post, user)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute("SELECT * FROM users ORDER BY created DESC;")
            for user in cursor.fetchall():
                users.append({"id": user.id, "username": user.username, "first_name": user.first_name,
                              "last_name": user.last_name, "password": user.password, "email": user.email,
                              "type": user.type, "display_name": user.display_name, "twitter":user.twitter,
                              "twitter_img": user.twitter_img, "color": user.color, "description": user.description,
                              "avatar": user.avatar, "new": user.new, "city": user.city, "is_active": user.is_active,
                              "created": user.created.isoformat()})
            data["users"] = users

        self.write(data)
        self.finish()

class AdminGetSitesHandler(BaseHandler):
    """
    Get all sites
    """
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminGetSitesHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        data, sites = {}, []
        print('AdminTestHandler', post, user)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            cursor = yield connection.execute("SELECT * FROM base_site ORDER BY created DESC;")
            for site in cursor.fetchall():
                sites.append({"id": site.id, "name": site.name, "description": site.description, "rss": site.rss,
                              "url": site.url, "short_url": site.short_url, "image": site.image,
                              "favicon": site.favicon, "color": site.color, "is_active": site.is_active,
                              "created": site.created.isoformat()})
            data["sites"] = sites

        self.write(data)
        self.finish()

class AdminCreateSitesHandler(BaseHandler):
    """
    Get all sites
    """
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminCreateSitesHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        data, sites = {}, []
        print('AdminTestHandler', post, user)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            now = datetime.datetime.now()
            cursor = yield connection.execute(
                            u"INSERT INTO base_site (is_active, created) VALUES ({0}, '{1}') RETURNING id, name, description, rss, url, short_url, image, favicon, color, is_active, created;".format(
                                    False, now))

            site = cursor.fetchone()
            # id, name, description, rss, url, short_url, image, favicon, color, is_active, created

            data['site'] = {"id": site.id, "name": site.name, "description": site.description, "rss": site.rss,
                              "url": site.url, "short_url": site.short_url, "image": site.image,
                              "favicon": site.favicon, "color": site.color, "is_active": site.is_active,
                              "created": site.created.isoformat()}

        self.write(data)
        self.finish()

class AdminUpdateSitesHandler(BaseHandler):
    """
    Get all sites
    """
    def __init__(self, *args, **kwargs):
        # self.http_connection_closed = False
        super(AdminUpdateSitesHandler, self).__init__(*args, **kwargs)

    @gen.coroutine
    @authenticateded
    def post(self):
        post = json.loads(self.request.body.decode('utf-8'))
        user = self.current_user
        data, sites = {}, []
        print('AdminUpdateSitesHandler', post, user)

        connection = yield self.db.getconn()
        with self.db.manage(connection):
            now = datetime.datetime.now()
            cursor = yield connection.execute(
                            u"UPDATE base_site SET name = '{1}', description = '{2}' , color = '{3}' , short_url = '{4}' , image = '{5}' , is_active = {6} , favicon = '{7}' , url = '{8}' , rss = '{9}' WHERE id = {0};".format(
                                    post['id'], post['name'], post['description'], post['color'], post['short_url'], post['image'], post['is_active'], post['favicon'], post['url'], post['rss'],))

            name_task = 'create_links ' + str(post['id'])
            cursor = yield connection.execute("SELECT * FROM djcelery_periodictask WHERE name='{0}';".format(name_task))

            tasks = cursor.fetchall()
            print('tasks', tasks)

        self.write(data)

        self.finish()

def main():
    try:
        tornado.options.parse_command_line()
        application = tornado.web.Application([
            (r'/', OverviewHandler),
            (r'/mogrify', MogrifyHandler),
            (r'/query', SingleQueryHandler),
            (r'/hstore', HstoreQueryHandler),
            (r'/json', JsonQueryHandler),
            (r'/transaction', TransactionHandler),
            (r'/multi_query', MultiQueryHandler),
            (r'/connection', ConnectionQueryHandler),

            (r'/api/v1/wall/filter/', UserWallFilterHandler),
            (r'/api/v1/wall/sites/', UserWallSitesHandler),
            (r'/api/v1/wall/trends/', UserWallTrendsHandler),
            (r'/api/v1/wall/top/', UserWallTopHandler),
            (r'/api/v1/wall/walls/', UserWallWallsHandler),
            (r'/api/v1/wall/category/', UserWallCategoryHandler),
            (r'/api/v1/wall/', UserWallHandler),
            (r'/api/v1/wall/list/', UserWallListHandler),
            (r'/api/v1/wall/slug/', UserSlugWallHandler),
            (r'/api/v1/wall/settings/', UserSettingsWallHandler),
            (r'/api/v1/wall/welcome/', UserWelcomeWallHandler),

            (r'/api/v1/column/create/', UserCreateColumnHandler),
            (r'/api/v1/column/get/letters/', UserCreateColumnLetterHandler),

            (r'/api/v1/column/settings/size/', UserChangeSizeColumnHandler),
            (r'/api/v1/column/settings/realtime/', UserChangeRealtimeColumnHandler),
            (r'/api/v1/column/settings/sort/', UserChangeSortColumnHandler),
            (r'/api/v1/column/settings/delete/', UserDeleteColumnHandler),
            (r'/api/v1/column/settings/words/', UserChangeWordsColumnHandler),
            (r'/api/v1/column/settings/edit/w/source/', UserChangeSourceEditWColumnHandler),
            (r'/api/v1/column/settings/get/w/source/', UserChangeSourceWColumnHandler),
            (r'/api/v1/column/settings/get/w/walls/', UserChangeWallsWColumnHandler),
            (r'/api/v1/column/settings/edit/w/walls/', UserChangeWallsEditWColumnHandler),

            (r'/api/v1/column/top/', UserTopColumnHandler),

            (r'/api/v1/column/preloader/', UserPreloaderColumnHandler),

            (r'/api/v1/column/create/related/', UserRelatedCreateHandler),
            (r'/api/v1/column/recontent/', UserReContentHandler),
            (r'/api/v1/column/favourite/', UserFavouriteHandler),

            (r'/api/v1/column/get/recontent/', UserGetReContentHandler),
            (r'/api/v1/column/get/favourite/', UserGetFavouriteHandler),

            (r'/api/v1/column/get/related/', UserGetRelatedHandler),

            (r'/api/v1/post/recontent/add/', UserPostReContentAddHandler),

            # user
            (r'/api/v1/user/get/', UserGetHandler),
            (r'/api/v1/user/get/settings/notifications/', UserGetSetNotificationsHandler),
            (r'/api/v1/user/settings/types/', UserSetTypesHandler),
            (r'/api/v1/user/settings/color/', UserSetColorHandler),
            (r'/api/v1/user/settings/picture/', UserSetPictureHandler),
            (r'/api/v1/user/follow/', UserFollowHandler),
            (r'/api/auth/twitter', UserAuthTwitterHandler),

            # WebSocket
            (r'/api/v1/ws/test/', WebSocketHandlerTest),
            (r'/api/v1/wall/create/', UserCreateWallHandler),

            # register new
            (r'/api/me', UserGetMeHandler),

            # Welcome UserWelcomeHandler
            (r'/api/v1/welcome/', UserWelcomeHandler),

            #ADMIN
            (r'/api/v1/admin/test/', AdminTestHandler),
            (r'/api/v1/admin/get/users/', AdminGetUsersHandler),
            (r'/api/v1/admin/get/sites/', AdminGetSitesHandler),

            (r'/api/v1/admin/create/sites/', AdminCreateSitesHandler),
            (r'/api/v1/admin/update/sites/', AdminUpdateSitesHandler),

            (r'/api/v1/admin/create/column/trend/', AdminCreateColumnTrendHandler),
            (r'/api/v1/admin/toggle/column/trend/', AdminToggleColumnTrendHandler),
            (r'/api/v1/admin/name/column/trend/', AdminNameColumnTrendHandler),

            (r'/api/v1/admin/toggle/wall/top/', AdminToggleWallTopHandler),
            (r'/api/v1/admin/create/wall/top/', AdminCreateWallTopHandler),
            (r'/api/v1/admin/create/column/top/', AdminCreateColumnTopHandler),
            (r'/api/v1/admin/toggle/column/top/', AdminToggleColumnTopHandler),
            (r'/api/v1/admin/name/column/top/', AdminNameColumnTopHandler),

            (r'/api/v1/admin/create/category/walls/', AdminCreateCategoryWallsHandler),
            (r'/api/v1/admin/toggle/category/walls/', AdminToggleCategoryWallsHandler),
            (r'/api/v1/admin/create/wall/walls/', AdminCreateWallWallsHandler),



            # (r".*", FallbackHandler, dict(fallback=tr)),

        ], debug=True, xsrf_cookies=False,
                cookie_secret="61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=")

        ioloop = tornado.ioloop.IOLoop.instance()

        application.db = momoko.Pool(
                dsn=dsn,
                size=4,
                max_size=6,
                cursor_factory=NamedTupleCursor,
                ioloop=ioloop,
                setsession=("SET TIME ZONE UTC",),
                raise_connect_errors=False,

        )

        # this is a one way to run ioloop in syncconnection = yield self.db.getconn()
        future = application.db.connect()
        ioloop.add_future(future, lambda f: ioloop.stop())
        ioloop.add_handler(conn.fileno(), receive, ioloop.READ)
        ioloop.start()

        if enable_hstore:
            future = application.db.register_hstore()
            # This is the other way to run ioloop in sync
            ioloop.run_sync(lambda: future)

        if application.db.server_version >= 90200:
            future = application.db.register_json()
            # This is the other way to run ioloop in sync
            ioloop.run_sync(lambda: future)

        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(7070, 'localhost')
        curs = conn.cursor()
        curs.execute("LISTEN users;")
        ioloop.start()
    except KeyboardInterrupt:
        print('Exit')


if __name__ == '__main__':
    main()
